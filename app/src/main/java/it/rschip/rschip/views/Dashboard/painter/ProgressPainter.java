package it.rschip.rschip.views.Dashboard.painter;

public interface ProgressPainter extends Painter {

    void setMax(float max);

    void setMin(float min);

    void setValue(float value);

    void change(int x1, int y1, int colorStart, int colorEnd);
}
