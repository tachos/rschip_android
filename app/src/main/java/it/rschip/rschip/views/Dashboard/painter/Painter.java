package it.rschip.rschip.views.Dashboard.painter;

import android.graphics.Canvas;

public interface Painter {

    void draw(Canvas canvas);

    int getColor();

    void setColor(int color);

    void onSizeChanged(int height, int width);
}
