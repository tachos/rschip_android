package it.rschip.rschip.views;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import it.rschip.rschip.R;

/**
 * Created by Dan on 18.05.2016.
 */
public class CustomDialog extends DialogFragment {

    TextView passwordDescription;

    private String mTitleText;
    private String mMessageText;
    private String mLeftButtonText;
    private String mRightButtonText;
    private String mCenterButtonText;
    private String mDescriptionText;

    private View.OnClickListener mLeftButtonListener;
    private View.OnClickListener mRightButtonListener;
    private View.OnClickListener mCenterButtonListener;
    private OnClickConfirmListener mConfirmListener;

    public CustomDialog setTitle(String title) {
        this.mTitleText = title;
        return this;
    }

    public CustomDialog setMessage(String message) {
        this.mMessageText = message;
        return this;
    }

    public CustomDialog setLeftButton(String name, View.OnClickListener listener) {
        this.mLeftButtonText = name;
        this.mLeftButtonListener = listener;
        return this;
    }

    public CustomDialog setRightButton(String name, View.OnClickListener listener) {
        this.mRightButtonText = name;
        this.mRightButtonListener = listener;
        return this;
    }

    public CustomDialog setCenterButton(String name, View.OnClickListener listener) {
        this.mCenterButtonText = name;
        this.mCenterButtonListener = listener;
        return this;
    }

    public CustomDialog setPasswordView(String description, OnClickConfirmListener listener) {
        this.mDescriptionText = description;
        this.mConfirmListener = listener;
        return this;
    }

    public void setDescriptionText(String description, final boolean error) {
        this.mDescriptionText = description;
        passwordDescription.post(new Runnable() {
            @Override
            public void run() {
                passwordDescription.setText(mDescriptionText);
                passwordDescription.setTextColor(ContextCompat.getColor(getContext(), error ? R.color.red : R.color.gray));
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_dialog, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView titleView = (TextView) view.findViewById(R.id.title);
        TextView messageView = (TextView) view.findViewById(R.id.message);
        Button leftBtn = (Button) view.findViewById(R.id.left_btn);
        Button rightBtn = (Button) view.findViewById(R.id.right_btn);
        View passwordView = view.findViewById(R.id.passwordView);
        final EditText password = (EditText) view.findViewById(R.id.password);
        passwordDescription = (TextView) view.findViewById(R.id.passwordDescription);
        if (mTitleText != null) {
            titleView.setText(mTitleText);
        } else {
            titleView.setVisibility(View.GONE);
        }
        if (mMessageText != null) {
            messageView.setText(mMessageText);
        } else {
            messageView.setVisibility(View.GONE);
            passwordView.setVisibility(View.VISIBLE);
            passwordDescription.setText(mDescriptionText);
        }
        if (mCenterButtonText != null) {
            leftBtn.setText(mCenterButtonText);
            leftBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCenterButtonListener != null) mCenterButtonListener.onClick(v);
                    else dismiss();
                }
            });
            rightBtn.setVisibility(View.GONE);
        } else {
            if (mRightButtonText != null) {
                rightBtn.setText(mRightButtonText);
                rightBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mMessageText != null) {
                            if (mRightButtonListener != null) mRightButtonListener.onClick(v);
                            else
                                dismiss();
                        } else {
                            if (mConfirmListener != null)
                                mConfirmListener.OnClickConfirm(password.getText().toString().trim());
                            else
                                dismiss();
                        }
                    }
                });
            } else {
                rightBtn.setVisibility(View.GONE);
            }
            if (mLeftButtonText != null) {
                leftBtn.setText(mLeftButtonText);
                leftBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mLeftButtonListener != null) mLeftButtonListener.onClick(v);
                        dismiss();
                    }
                });
            } else {
                leftBtn.setVisibility(View.GONE);
            }
        }
        return view;
    }

    public interface OnClickConfirmListener {
        void OnClickConfirm(String password);
    }
}
