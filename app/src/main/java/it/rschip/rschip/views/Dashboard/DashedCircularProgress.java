package it.rschip.rschip.views.Dashboard;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.RelativeLayout;

import it.rschip.rschip.R;
import it.rschip.rschip.views.Dashboard.painter.InternalCirclePainterImp;
import it.rschip.rschip.views.Dashboard.painter.ProgressPainter;
import it.rschip.rschip.views.Dashboard.painter.ProgressPainterImp;

public class DashedCircularProgress extends RelativeLayout {

    private Context context;
    private InternalCirclePainterImp externalCirclePainter;
    private ProgressPainter progressPainter;
    private Interpolator interpolator = new AccelerateDecelerateInterpolator();
    private int externalColor = Color.RED;
    private int startColor = Color.YELLOW;
    private int endColor = Color.RED;
    private float min = 0;
    private float last = min;
    private float max = 100;
    private ValueAnimator valueAnimator;
    private OnAnimChangeListener animChangeListener;
    private float value;
    private int duration = 1500;
    private int progressStrokeWidth = 48;
    private int strokeWidth = 48;
    private int dashWith = 3;
    private int dashSpace = 8;
    private int dashWithProgress = 2;
    private int dashSpaceProgress = 6;
    private int lastValue = 0;
    private boolean animating = false;

    public DashedCircularProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DashedCircularProgress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setWillNotDraw(false);
        this.context = context;
        this.dashWith = context.getResources().getDimensionPixelSize(R.dimen.dashWith);
        this.dashSpace = context.getResources().getDimensionPixelSize(R.dimen.dashSpace);
        this.dashWithProgress = context.getResources().getDimensionPixelSize(R.dimen.dashWithProgress);
        this.dashSpaceProgress = context.getResources().getDimensionPixelSize(R.dimen.dashSpaceProgress);
        this.progressStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.progressStrokeWidth);
        this.strokeWidth = context.getResources().getDimensionPixelSize(R.dimen.strokeWidth);
        initPainters();
        initValueAnimator();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        progressPainter.onSizeChanged(h, w);
        externalCirclePainter.onSizeChanged(h, w);
        animateValue();
    }

    private void initPainters() {
        progressPainter = new ProgressPainterImp(dashWithProgress, dashSpaceProgress, startColor, endColor, min, max, progressStrokeWidth, progressStrokeWidth);
        externalCirclePainter = new InternalCirclePainterImp(dashWith, dashSpace, externalColor, strokeWidth);
    }

    private void initValueAnimator() {
        valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(interpolator);
        valueAnimator.addUpdateListener(new ValueAnimatorListenerImp());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        externalCirclePainter.draw(canvas);
        progressPainter.draw(canvas);
        invalidate();
    }

    public void setValue(float value) {
        this.value = value;
        if (value <= max || value >= min) {
            animateValue();
        }
    }

    private void animateValue() {
        if (valueAnimator != null) {
            animating = true;
            if (animChangeListener != null)
                animChangeListener.onAnimStarted();
            valueAnimator.setFloatValues(last, value);
            valueAnimator.setDuration(duration);
            valueAnimator.start();
        }
    }

    public void off() {
        this.externalCirclePainter.setColor(ContextCompat.getColor(context, R.color.gray));
        setValue(0);
    }

    public void on() {
        this.externalCirclePainter.setColor(externalColor);
        setValue(lastValue);
    }

    public void setSportPlus() {
        this.externalColor = ContextCompat.getColor(context, R.color.red);
        this.startColor = ContextCompat.getColor(context, R.color.orange);
        this.endColor = ContextCompat.getColor(context, R.color.orange_sport_plus_end);
        this.progressPainter.change(0, 1000, startColor, endColor);
        this.externalCirclePainter.setColor(externalColor);
        setValue(60);
        lastValue = 60;
    }

    public void setSport() {
        this.externalColor = ContextCompat.getColor(context, R.color.orange_sport);
        this.startColor = ContextCompat.getColor(context, R.color.orange);
        this.endColor = ContextCompat.getColor(context, R.color.orange_sport_end);
        this.progressPainter.change(0, 1000, startColor, endColor);
        this.externalCirclePainter.setColor(externalColor);
        setValue(40);
        lastValue = 40;
    }

    public void setEco() {
        this.externalColor = ContextCompat.getColor(context, R.color.green_eco);
        this.startColor = ContextCompat.getColor(context, R.color.orange);
        this.endColor = ContextCompat.getColor(context, R.color.green_eco_end);
        this.progressPainter.change(1000, 1000, startColor, endColor);
        this.externalCirclePainter.setColor(externalColor);
        setValue(20);
        lastValue = 20;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    public void setAnimChangeListener(OnAnimChangeListener valueChangeListener) {
        this.animChangeListener = valueChangeListener;
    }

    public void setInterpolator(Interpolator interpolator) {
        this.interpolator = interpolator;

        if (valueAnimator != null) {
            valueAnimator.setInterpolator(interpolator);
        }
    }

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
        progressPainter.setMin(min);
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
        progressPainter.setMax(max);
    }

    public void reset() {
        last = min;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isAnimating() {
        return animating;
    }

    public interface OnAnimChangeListener {
        void onValueChange(float value);

        void onAnimStarted();

        void onAnimEnded();
    }

    private class ValueAnimatorListenerImp implements ValueAnimator.AnimatorUpdateListener {
        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            Float newValue = (Float) valueAnimator.getAnimatedValue();
            progressPainter.setValue(newValue);
            if (animChangeListener != null) {
                animChangeListener.onValueChange(newValue);
            }
            last = newValue;
            if (last == value) {
                animating = false;
                if (animChangeListener != null)
                    animChangeListener.onAnimEnded();
            }
        }
    }


}
