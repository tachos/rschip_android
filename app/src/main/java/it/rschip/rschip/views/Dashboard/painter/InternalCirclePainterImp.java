package it.rschip.rschip.views.Dashboard.painter;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;

public class InternalCirclePainterImp implements Painter {

    private RectF internalCircle;
    private RectF internalCircleBg;
    private Paint internalCirclePaint;
    private Paint internalBgPaint;
    private int color;
    private float startAngle = 0;
    private float finishAngle = 359.8f;
    private int width = 0;
    private int height = 0;
    private int internalStrokeWidth = 48;
    private int dashWith = 2;
    private int dashSpace = 4;

    public InternalCirclePainterImp(int dashWith, int dashSpace, int color, int internalStrokeWidth) {
        this.internalStrokeWidth = internalStrokeWidth;
        this.color = color;
        this.dashWith = dashWith;
        this.dashSpace = dashSpace;
        init();
    }

    private void init() {
        initExternalCirclePainter();
    }

    private void initExternalCirclePainter() {
        internalCirclePaint = new Paint();
        internalCirclePaint.setAntiAlias(true);
        internalCirclePaint.setStrokeWidth(internalStrokeWidth);
        internalCirclePaint.setColor(color);
        internalCirclePaint.setStyle(Paint.Style.STROKE);
        internalCirclePaint.setPathEffect(new DashPathEffect(new float[]{dashWith, dashSpace}, dashSpace));

        internalBgPaint = new Paint();
        internalBgPaint.setAntiAlias(true);
        internalBgPaint.setStrokeWidth(internalStrokeWidth + internalStrokeWidth / 4);
        internalBgPaint.setStyle(Paint.Style.STROKE);
    }

    private void initExternalCircle() {
        internalCircle = new RectF();
        internalCircleBg = new RectF();
        float padding = internalStrokeWidth * 1.7f;
        float paddingbg = internalStrokeWidth * 1.6f;
        internalCircle.set(padding, padding, width - padding, height - padding);
        internalCircleBg.set(paddingbg, paddingbg, width - paddingbg, height - paddingbg);
        internalBgPaint.setShader(new RadialGradient(width / 2, height / 2, width / 2, color, Color.TRANSPARENT, Shader.TileMode.CLAMP));
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawArc(internalCircleBg, startAngle, finishAngle, false, internalBgPaint);
        canvas.drawArc(internalCircle, startAngle, finishAngle, false, internalCirclePaint);
    }

    @Override
    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        internalCirclePaint.setColor(color);
        if (width != 0) {
            internalBgPaint.setShader(new RadialGradient(width / 2, height / 2, width / 2, color, Color.TRANSPARENT, Shader.TileMode.CLAMP));
        }
    }

    @Override
    public void onSizeChanged(int height, int width) {
        this.width = width;
        this.height = height;
        initExternalCircle();
    }
}
