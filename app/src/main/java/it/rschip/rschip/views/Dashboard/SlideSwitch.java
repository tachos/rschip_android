package it.rschip.rschip.views.Dashboard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import it.rschip.rschip.R;


/**
 * Created by Dan on 31.05.2016.
 */
public class SlideSwitch extends View {

    private static final int RIM_SIZE = 6;
    // 3 attributes
    private int color_theme;
    private boolean isOpen;
    // varials of drawing
    private Paint fontPaint;
    private Paint paint;
    private Rect backRect;
    private Rect frontRect;
    private RectF frontCircleRect;
    private int padding = 0;
    private int alpha;
    private int max_left;
    private int min_left;
    private int frontRect_left;
    private int frontRect_left_begin = RIM_SIZE;
    private int eventStartX;
    private int eventLastX;
    private int diffX = 0;
    private boolean slideable = true;
    private SlideListener listener;
    private int red;
    private int green;
    private int text_size;
    private String off;
    private String on;

    public SlideSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        listener = null;
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(context.getResources().getDimensionPixelSize(R.dimen.strokeWidthButton));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        color_theme = Color.TRANSPARENT;
        isOpen = false;
        off = context.getResources().getString(R.string.off);
        on = context.getResources().getString(R.string.on);
        red = ContextCompat.getColor(context, R.color.red);
        green = ContextCompat.getColor(context, R.color.green_eco);
        fontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fontPaint.setTextAlign(Paint.Align.CENTER);
        fontPaint.setColor(red);
        padding = context.getResources().getDimensionPixelSize(R.dimen._30sdp);
        text_size = context.getResources().getDimensionPixelSize(R.dimen.switch_text_size);
        fontPaint.setTextSize(text_size);
        fontPaint.setStyle(Paint.Style.STROKE);
    }

    public SlideSwitch(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SlideSwitch(Context context) {
        this(context, null);
    }

    public boolean isSlideable() {
        return slideable;
    }

    public void setSlideable(boolean slideable) {
        this.slideable = slideable;
        if (!slideable) moveToDest(isOpen());
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = measureDimension(280, widthMeasureSpec);
        int height = measureDimension(140, heightMeasureSpec);
        if (width < height) width = height * 2;
        setMeasuredDimension(width, height);
        initDrawingVal();
    }

    public void initDrawingVal() {
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();

        frontCircleRect = new RectF();
        frontRect = new Rect();
        backRect = new Rect(0, 0, width, height);
        min_left = RIM_SIZE;
        max_left = width - (height - 2 * RIM_SIZE) - RIM_SIZE;
        if (isOpen) {
            frontRect_left = max_left;
            alpha = 255;
        } else {
            frontRect_left = RIM_SIZE;
            alpha = 0;
        }
        frontRect_left_begin = frontRect_left;
    }

    public int measureDimension(int defaultSize, int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = defaultSize; // UNSPECIFIED
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int radius = backRect.height() / 2 - RIM_SIZE;
        paint.setColor(Color.GRAY);
        paint.setColor(color_theme);
        paint.setAlpha(alpha);
        frontRect.set(frontRect_left, RIM_SIZE, frontRect_left + backRect.height() - 2 * RIM_SIZE, backRect.height() - RIM_SIZE);
        frontCircleRect.set(frontRect);
        paint.setColor(Color.WHITE);
        canvas.drawRoundRect(frontCircleRect, radius, radius, paint);
        if (isOpen) {
            fontPaint.setColor(green);
            canvas.drawText(on, frontRect_left + radius, (backRect.height() / 2.0f) - ((fontPaint.descent() + fontPaint.ascent()) / 2), fontPaint);
        } else {
            fontPaint.setColor(red);
            canvas.drawText(off, frontRect_left + radius, (backRect.height() / 2.0f) - ((fontPaint.descent() + fontPaint.ascent()) / 2), fontPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (slideable == false)
            return super.onTouchEvent(event);
        int action = MotionEventCompat.getActionMasked(event);
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                eventStartX = (int) event.getRawX();
                break;
            case MotionEvent.ACTION_MOVE:
                eventLastX = (int) event.getRawX();
                if ((isOpen && eventLastX >= (frontRect_left)) || (!isOpen && eventLastX <= (frontRect_left + backRect.height() + padding))) {
                    diffX = eventLastX - eventStartX;
                    int tempX = diffX + frontRect_left_begin;
                    tempX = (tempX > max_left ? max_left : tempX);
                    tempX = (tempX < min_left ? min_left : tempX);
                    if (tempX >= min_left && tempX <= max_left) {
                        frontRect_left = tempX;
                        alpha = (int) (255 * (float) tempX / (float) max_left);
                        invalidateView();
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                moveToDest(frontRect_left > max_left / 2);
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * draw again
     */
    private void invalidateView() {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            invalidate();
        } else {
            postInvalidate();
        }
    }

    public void setSlideListener(SlideListener listener) {
        this.listener = listener;
    }

    public void moveToDest(final boolean toRight) {
        ValueAnimator toDestAnim = ValueAnimator.ofInt(frontRect_left, toRight ? max_left : min_left);
        toDestAnim.setDuration(300);
        toDestAnim.setInterpolator(new AccelerateDecelerateInterpolator());
        toDestAnim.start();
        toDestAnim.addUpdateListener(new AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                frontRect_left = (Integer) animation.getAnimatedValue();
                alpha = (int) (255 * (float) frontRect_left / (float) max_left);
                invalidateView();
            }
        });
        if (isSlideable())
            toDestAnim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    if (toRight) {
                        isOpen = true;
                        if (listener != null)
                            listener.on();
                        frontRect_left_begin = max_left;
                    } else {
                        isOpen = false;
                        if (listener != null)
                            listener.off();
                        frontRect_left_begin = min_left;
                    }
                }
            });
    }

    public void setState(boolean isOpen) {
        this.isOpen = isOpen;
        initDrawingVal();
        invalidateView();
        if (listener != null)
            if (isOpen == true) {
                listener.on();
            } else {
                listener.off();
            }
    }

    public boolean isOpen() {
        return isOpen;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            this.isOpen = bundle.getBoolean("isOpen");
            state = bundle.getParcelable("instanceState");
        }
        super.onRestoreInstanceState(state);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putBoolean("isOpen", this.isOpen);
        return bundle;
    }

    public interface SlideListener {
        void on();

        void off();
    }
}