package it.rschip.rschip.views;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import it.rschip.rschip.R;

/**
 * Created by Dan on 01.06.2016.
 */
public class InfoDialogFragment extends DialogFragment {

    private boolean isDashboardInfo = false;

    public DialogFragment setType(boolean isDashboardInfo) {
        this.isDashboardInfo = isDashboardInfo;
        return this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.AppTheme);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.InfoDialogAnimation;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        if (isDashboardInfo) {
            ((TextView) view.findViewById(R.id.line1)).setText(getString(R.string.descriptionDashboardHelpLabel));
            ((TextView) view.findViewById(R.id.line2)).setText(Html.fromHtml(getString(R.string.sportPlusDashboardHelpLabel)));
            ((TextView) view.findViewById(R.id.line3)).setText(Html.fromHtml(getString(R.string.sportDashboardHelpLabel)));
            ((TextView) view.findViewById(R.id.line4)).setText(Html.fromHtml(getString(R.string.ecoDashboardHelpLabel)));
            ((TextView) view.findViewById(R.id.line5)).setText(Html.fromHtml(getString(R.string.powerDashboardHelpLabel)));
        }
        view.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }
}
