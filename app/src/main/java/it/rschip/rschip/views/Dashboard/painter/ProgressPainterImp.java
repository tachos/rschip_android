package it.rschip.rschip.views.Dashboard.painter;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;

public class ProgressPainterImp implements ProgressPainter {

    private RectF progressCircle;
    private Paint progressPaint;
    private int color = Color.YELLOW;
    private int colorEnd = Color.RED;
    private float startAngle = 90f;
    private float plusAngle = 0;
    private int internalStrokeWidth = 48;
    private int dashWith = 2;
    private int dashSpace = 6;
    private float padding;
    private float min;
    private float max;
    private int width;
    private int height;
    private int padding2;

    public ProgressPainterImp(int dashWith, int dashSpace, int colorStart, int colorEnd, float min, float max, int progressStrokeWidth, int padding) {
        this.color = colorStart;
        this.colorEnd = colorEnd;
        this.min = min;
        this.max = max;
        this.internalStrokeWidth = progressStrokeWidth;
        this.padding2 = padding;
        this.dashWith = dashWith;
        this.dashSpace = dashSpace;
        init();
    }

    private void init() {
        initInternalCirclePainter();
    }

    private void initInternalCirclePainter() {
        progressPaint = new Paint();
        progressPaint.setAntiAlias(true);
        progressPaint.setStrokeWidth(internalStrokeWidth);
        progressPaint.setShader(new LinearGradient(0, 0, 0, 1000, colorEnd, color, Shader.TileMode.CLAMP));
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setPathEffect(new DashPathEffect(new float[]{dashWith, dashSpace}, dashSpace));
    }

    private void initInternalCircle() {
        progressCircle = new RectF();
        padding = internalStrokeWidth * 1.7f + padding2;
        progressCircle.set(padding, padding, width - padding, height - padding);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawArc(progressCircle, startAngle, plusAngle, false, progressPaint);
    }

    @Override
    public void change(int x1, int y1, int colorStart, int colorEnd) {
        this.color = colorStart;
        this.colorEnd = colorEnd;
        this.progressPaint.setShader(new LinearGradient(0, 0, x1, y1, colorEnd, color, Shader.TileMode.CLAMP));
    }


    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public void setValue(float value) {
        this.plusAngle = (359.8f * value) / max;
    }

    @Override
    public void onSizeChanged(int height, int width) {
        this.width = width;
        this.height = height;
        initInternalCircle();
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        progressPaint.setColor(color);
    }
}
