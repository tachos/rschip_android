package it.rschip.rschip.models;

import java.util.List;

/**
 * Created by Dan on 26.05.2016.
 */
public class Car {
    private List<Item> data;

    public List<Item> getData() {
        return data;
    }

    public void setData(List<Item> data) {
        this.data = data;
    }
}
