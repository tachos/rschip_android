package it.rschip.rschip.models;

/**
 * Created by Dan on 18.08.2016.
 */
public class CheckUpgrade {
    String error;
    Data data;

    public String getError() {
        return error;
    }

    public Data getData() {
        return data;
    }

    public class Data {
        String link_to_android;

        public String getLink_to_android() {
            return link_to_android;
        }
    }
}
