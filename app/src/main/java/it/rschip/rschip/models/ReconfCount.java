package it.rschip.rschip.models;

/**
 * Created by Dan on 02.06.2016.
 */
public class ReconfCount {
    int reconf_count;

    public int getReconf_count() {
        return reconf_count;
    }

    public void setReconf_count(int reconf_count) {
        this.reconf_count = reconf_count;
    }
}
