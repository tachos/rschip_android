package it.rschip.rschip.models;

public class ModificationItem extends Item {
    private int hp;
    private int nm;

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getNm() {
        return nm;
    }

    public void setNm(int nm) {
        this.nm = nm;
    }
}
