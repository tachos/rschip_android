package it.rschip.rschip.models;

import java.util.List;

/**
 * Created by Dan on 27.05.2016.
 */
public class Modification {
    private List<ModificationItem> data;

    public String[] getTitles() {
        String[] titles = new String[data.size()];
        for (int i = 0; i < data.size(); i++) {
            titles[i] = data.get(i).getTitle() + " " + data.get(i).getHp() + "hp " + data.get(i).getNm() + "nm";
        }
        return titles;
    }

    public List<ModificationItem> getData() {
        return data;
    }

    public void setData(List<ModificationItem> data) {
        this.data = data;
    }
}

