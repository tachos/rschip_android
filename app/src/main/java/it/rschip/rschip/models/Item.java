package it.rschip.rschip.models;

/**
 * Created by Dan on 26.05.2016.
 */
public class Item {
    private int id;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
