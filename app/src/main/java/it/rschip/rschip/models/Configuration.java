package it.rschip.rschip.models;

/**
 * Created by Dan on 02.06.2016.
 */
public class Configuration {
    String serial_number;
    String device_id;
    String modification_id;
    String device_type;

    public Configuration(String serial_number, String device_id, String modification_id, String device_type) {
        this.serial_number = serial_number;
        this.device_id = device_id;
        this.modification_id = modification_id;
        this.device_type = device_type;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getModification_id() {
        return modification_id;
    }

    public void setModification_id(String modification_id) {
        this.modification_id = modification_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }
}
