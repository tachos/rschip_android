package it.rschip.rschip.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import it.rschip.rschip.R;
import it.rschip.rschip.models.Dealer;

/**
 * Created by Dan on 23.05.2016.
 */
public class DealersAdapter extends ArrayAdapter<Dealer> {

    private LayoutInflater inflater = null;

    public DealersAdapter(Context context, List<Dealer> items) {
        super(context, 0, items);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_dealer, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.description = (TextView) convertView.findViewById(R.id.description);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(getItem(position).getName());
        holder.description.setText(getItem(position).getShort_description());
        return convertView;
    }

    private class ViewHolder {
        TextView name;
        TextView description;
    }
}
