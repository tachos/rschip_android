package it.rschip.rschip.adapters;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import it.rschip.rschip.R;

/**
 * Created by Dan on 20.05.2016.
 */
public class RsChipAdapter extends ArrayAdapter<BluetoothDevice> {

    ArrayList<BluetoothDevice> items;
    private LayoutInflater inflater = null;
    private int selectedPosition = -1;

    public RsChipAdapter(Context context, ArrayList<BluetoothDevice> items) {
        super(context, 0, items);
        this.items = items;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public BluetoothDevice getSelectedItem() {
        return items.get(selectedPosition);
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
    }

    public boolean contains(BluetoothDevice device) {
        for (BluetoothDevice dev : items) {
            if (device.getAddress().equals(dev.getAddress())) return true;
        }
        return false;
    }

    @SuppressLint("MissingPermission")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_rschip, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.select = (AppCompatRadioButton) convertView.findViewById(R.id.selectChip);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(getItem(position).getName());
        holder.select.setChecked(selectedPosition == position);
        return convertView;
    }

    private class ViewHolder {
        TextView name;
        AppCompatRadioButton select;
    }
}
