package it.rschip.rschip.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import it.rschip.rschip.App;
import it.rschip.rschip.R;
import it.rschip.rschip.bluetooth.BluetoothConnection;
import it.rschip.rschip.utils.Utils;

/**
 * Created by Dan on 17.05.2016.
 */
public class HomeAdapter extends ArrayAdapter<String> {

    private LayoutInflater inflater = null;
    private int[] height_padding;
    private String[] icons;

    public HomeAdapter(Context context) {
        super(context, 0, context.getResources().getStringArray(R.array.items_home_menu));
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.icons = context.getResources().getStringArray(R.array.icons);
        this.height_padding = Utils.getHeightAndPadding(context);
    }

    @Override
    public int getItemViewType(int position) {
        return position < 5 ? 0 : 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            if (getItemViewType(position) == 0) {
                convertView = inflater.inflate(R.layout.item_home_menu, parent, false);
            } else {
                convertView = inflater.inflate(R.layout.item_footer, null);
            }
        }
        if (getItemViewType(position) == 0) {
            convertView.findViewById(R.id.rootView).getLayoutParams().height = height_padding[0];
            if (position == 0) convertView.setPadding(0, height_padding[1], 0, 0);
            if (position == 4) convertView.setPadding(0, 0, 0, height_padding[1]);
            if (App.get().isDemo() && position == 0)
                ((TextView) convertView.findViewById(R.id.text)).setText(getContext().getString(R.string.exit_demo));
            else if (!BluetoothConnection.getLastMac().isEmpty() && position == 2)
                ((TextView) convertView.findViewById(R.id.text)).setText(R.string.change_auto);
            else
                ((TextView) convertView.findViewById(R.id.text)).setText(getItem(position));
            ((ImageView) convertView.findViewById(R.id.img)).setImageResource(getContext().getResources().getIdentifier(icons[position], "drawable", getContext().getPackageName()));
        }
        return convertView;
    }
}
