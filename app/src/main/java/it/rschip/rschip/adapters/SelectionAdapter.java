package it.rschip.rschip.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import it.rschip.rschip.R;

/**
 * Created by Dan on 25.05.2016.
 */
public class SelectionAdapter extends ArrayAdapter<String> {

    private LayoutInflater inflater = null;

    private int selectedPosition = -1;

    public SelectionAdapter(Context context, String[] items, int selected) {
        super(context, 0, items);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.selectedPosition = selected;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_language, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.select = (AppCompatRadioButton) convertView.findViewById(R.id.selectChip);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(getItem(position));
        holder.select.setChecked(selectedPosition == position);
        return convertView;
    }

    private class ViewHolder {
        TextView name;
        AppCompatRadioButton select;
    }
}