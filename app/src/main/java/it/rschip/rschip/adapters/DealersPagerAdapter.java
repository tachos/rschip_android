package it.rschip.rschip.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import it.rschip.rschip.ui.fragments.DealersListFragment;
import it.rschip.rschip.ui.fragments.DealersMapFragment;

/**
 * Created by Dan on 23.05.2016.
 */
public class DealersPagerAdapter extends FragmentPagerAdapter {

    public DealersPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new DealersListFragment();
        } else {
            return new DealersMapFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
