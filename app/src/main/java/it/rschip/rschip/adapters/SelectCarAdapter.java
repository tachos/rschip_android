package it.rschip.rschip.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import it.rschip.rschip.R;
import it.rschip.rschip.models.Item;

/**
 * Created by Dan on 26.05.2016.
 */
public class SelectCarAdapter extends ArrayAdapter<Item> {

    private LayoutInflater inflater = null;

    public SelectCarAdapter(Context context, List<Item> items) {
        super(context, 0, items);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_select_car, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(getItem(position).getTitle());
        return convertView;
    }

    private class ViewHolder {
        TextView name;
    }
}