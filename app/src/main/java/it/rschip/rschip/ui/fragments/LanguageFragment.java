package it.rschip.rschip.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import it.rschip.rschip.App;
import it.rschip.rschip.R;
import it.rschip.rschip.adapters.SelectionAdapter;
import it.rschip.rschip.utils.LocaleHelper;
import it.rschip.rschip.utils.SlidingMenuHelper;
import it.rschip.rschip.utils.ToolbarHelper;

/**
 * Created by Dan on 25.05.2016.
 */
public class LanguageFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_language, null);
        final TextView description = (TextView) view.findViewById(R.id.description);
        ListView listView = (ListView) view.findViewById(R.id.listView);
        final SelectionAdapter adapter = new SelectionAdapter(view.getContext(), getResources().getStringArray(R.array.languages), App.get().getPositionLanguage());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        LocaleHelper.get().setLocale(getActivity(), "en", "");
                        break;
                    case 1:
                        LocaleHelper.get().setLocale(getActivity(), "es", "");
                        break;
                    case 2:
                        LocaleHelper.get().setLocale(getActivity(), "zh", "CN");
                        break;
                    case 3:
                        LocaleHelper.get().setLocale(getActivity(), "ru", "");
                        break;
                }

                ToolbarHelper.getInstance().showBack(getString(R.string.language));
                description.setText(getString(R.string.lang_description));
                SlidingMenuHelper.getInstance().updateViews();

                App.get().setLanguage(position);
                adapter.setSelectedPosition(position);
                adapter.notifyDataSetChanged();
            }
        });
        return view;
    }
}
