package it.rschip.rschip.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import it.rschip.rschip.App;
import it.rschip.rschip.R;
import it.rschip.rschip.bluetooth.BluetoothConnection;
import it.rschip.rschip.ui.fragments.AboutFragment;
import it.rschip.rschip.ui.fragments.ActivateFragment;
import it.rschip.rschip.ui.fragments.BaseFragment;
import it.rschip.rschip.ui.fragments.ContactUsFragment;
import it.rschip.rschip.ui.fragments.DashboardFragment;
import it.rschip.rschip.ui.fragments.DealersFragment;
import it.rschip.rschip.ui.fragments.HomeFragment;
import it.rschip.rschip.ui.fragments.SettingsFragment;
import it.rschip.rschip.ui.fragments.SupportFragment;
import it.rschip.rschip.utils.SlidingMenuHelper;
import it.rschip.rschip.utils.ToolbarHelper;
import it.rschip.rschip.utils.TransactionFragment;
import it.rschip.rschip.utils.TransactionHomeFragment;
import it.rschip.rschip.utils.Utils;

public class MainActivity extends FragmentActivity {

    private ToolbarHelper mToolbarHelper;
    private SlidingMenuHelper mSlidingMenuHelper;
    public View.OnClickListener onMenuItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mSlidingMenuHelper.hide();
            if (mSlidingMenuHelper.isClick(v.getId())) {
                BaseFragment fragment = null;
                switch (v.getId()) {
                    case R.id.exit_demo:
                        App.get().setDemo(false);
                    case R.id.home:
                        fragment = new HomeFragment();
                        mSlidingMenuHelper.enabled(false);
                        break;
                    case R.id.dashboard:
                        fragment = new DashboardFragment();
                        break;
                    case R.id.settings:
                        fragment = new SettingsFragment();
                        break;
                    case R.id.contact_us:
                        mToolbarHelper.showMainMenu("");
                        fragment = new ContactUsFragment();
                        break;
                }
                if (fragment != null) {
                    getSupportFragmentManager().popBackStack();
                    getSupportFragmentManager().beginTransaction().setCustomAnimations(0, 0, R.anim.in_left, R.anim.out_right).replace(R.id.fragment_container, fragment, "FullScreenFragment").addToBackStack(null).commit();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSlidingMenuHelper = new SlidingMenuHelper(this, findViewById(android.R.id.content), onMenuItemClickListener);
        mToolbarHelper = new ToolbarHelper(this, findViewById(android.R.id.content));
        mToolbarHelper.setClickListenerMenu(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSlidingMenuHelper.show();
            }
        });
        mToolbarHelper.setClickListenerBack(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (BluetoothConnection.getLastMac().isEmpty()) {
            //No devices attached
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment(), "FullScreenFragment").commit();
        } else {
            //device attached
            App.get().setDemo(false);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DashboardFragment(), "FullScreenFragment").commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onEvent(final TransactionFragment event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mToolbarHelper.showBack(event.getTitle());
                if (event.getFragment() instanceof HomeFragment)
                    getSupportFragmentManager().popBackStack();
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.in_right, R.anim.out_left, R.anim.in_left, R.anim.out_right)
                        .replace(R.id.fragment_container, event.getFragment(), "FullScreenFragment")
                        .addToBackStack(null).commit();
            }
        });
    }

    @Subscribe
    public void onEvent(TransactionHomeFragment event) {
        BaseFragment fragment = null;
        switch (event.getPosition()) {
            case 0:
                fragment = new DashboardFragment();
                break;
            case 1:
                fragment = new AboutFragment();
                break;
            case 2:
                fragment = new ActivateFragment();
                break;
            case 3:
                fragment = new SupportFragment();
                break;
            case 4:
                fragment = new DealersFragment();
                break;
        }
        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.in_right, R.anim.out_left, R.anim.in_left, R.anim.out_right)
                    .replace(R.id.fragment_container, fragment, "FullScreenFragment");
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentByTag("FullScreenFragment") != null && getSupportFragmentManager().findFragmentByTag("FullScreenFragment") instanceof HomeFragment) {
            finish();
        }
        Utils.backSelectionCarStage();
        if (mSlidingMenuHelper.isShowing()) {
            mSlidingMenuHelper.hide();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                mSlidingMenuHelper.enabled(false);
            }
            super.onBackPressed();
        }
    }
}
