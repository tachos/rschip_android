package it.rschip.rschip.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import it.rschip.rschip.R;
import it.rschip.rschip.utils.ToolbarHelper;
import it.rschip.rschip.utils.TransactionFragment;

/**
 * Created by Dan on 19.05.2016.
 */
public class AboutFragment extends BaseFragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_about, null);
        view.findViewById(R.id.what_is_rschip).setOnClickListener(this);
        view.findViewById(R.id.start_guide).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        AboutWebFragment fragment = new AboutWebFragment();
        Bundle bundle = new Bundle();
        String title = "";
        switch (v.getId()) {
            case R.id.what_is_rschip:
                title = getString(R.string.what_is_rschip);
                bundle.putString("url", getString(R.string.whatIsLink));
                break;
            case R.id.start_guide:
                title = getString(R.string.start_guide);
                bundle.putString("url", getString(R.string.quickStartLink));
                break;
        }
        fragment.setArguments(bundle);
        EventBus.getDefault().post(new TransactionFragment(fragment, title));
    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarHelper.getInstance().showBack(getString(R.string.AboutRSCHIP));
    }
}
