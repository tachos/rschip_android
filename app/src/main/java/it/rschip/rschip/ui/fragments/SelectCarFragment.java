package it.rschip.rschip.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Locale;

import it.rschip.rschip.App;
import it.rschip.rschip.R;
import it.rschip.rschip.adapters.SelectCarAdapter;
import it.rschip.rschip.adapters.SelectionAdapter;
import it.rschip.rschip.bluetooth.BluetoothConnection;
import it.rschip.rschip.bluetooth.events.EventBluetoothStatusReceived;
import it.rschip.rschip.models.Car;
import it.rschip.rschip.models.Item;
import it.rschip.rschip.models.Modification;
import it.rschip.rschip.models.ModificationItem;
import it.rschip.rschip.models.Settings;
import it.rschip.rschip.utils.RestHelper;
import it.rschip.rschip.utils.ToolbarHelper;
import it.rschip.rschip.utils.TransactionFragment;
import it.rschip.rschip.utils.TransactionHomeFragment;
import it.rschip.rschip.utils.Utils;
import it.rschip.rschip.views.CustomDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dan on 26.05.2016.
 */
public class SelectCarFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    private Context context;
    private ListView listView;
    private TextView description;
    private Button bottomButton;
    private View bottomView;

    private List<Item> items;
    private List<ModificationItem> modifications;

    private SelectionAdapter modificationsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.fragment_language, null);
        context = view.getContext();
        bottomView = view.findViewById(R.id.bottomView);
        bottomButton = (Button) view.findViewById(R.id.bottomButton);
        listView = (ListView) view.findViewById(R.id.listView);
        listView.setOnItemClickListener(this);
        description = (TextView) view.findViewById(R.id.description);
        App.get().setSelectionMode(true);
        switch (App.get().getSelectionStage()) {
            case 0:
                ToolbarHelper.getInstance().setTitle(getString(R.string.select_car));
                description.setText(getString(R.string.select_car_descr));
                getMarks();
                break;
            case 1:
                ToolbarHelper.getInstance().setTitle(getString(R.string.select_model));
                description.setText(App.get().getMark());
                getModels(App.get().getMarkId());
                break;
            case 2:
                ToolbarHelper.getInstance().setTitle(getString(R.string.select_modification));
                description.setText(App.get().getMark() + " " + App.get().getModel());
                getModifications(App.get().getModelId());
                break;
        }
        bottomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CustomDialog dialog = new CustomDialog();

                dialog.setMessage(context.getString(R.string.select_car_confirm));
                dialog.setLeftButton(context.getString(R.string.confirm), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        App.get().setModification(modifications.get(modificationsAdapter.getSelectedPosition()));
                        showProgressDialog(null);

                        RestHelper.getInstance().RegistrationConfiguration(
                                BluetoothConnection.getDefaultInstance().getDeviceId(),
                                String.valueOf(App.get().getModification().getId()),
                                new RestHelper.CallbackStatus() {
                                    @Override
                                    public void result(boolean status) {
                                        if (status) {
                                            RestHelper.getInstance().getSettings(App.get().getModification().getId(), new RestHelper.CallbackFile() {
                                                @Override
                                                public void result(String content, Settings.Item item) {
                                                    if (content.isEmpty()) {
                                                        Toast.makeText(getContext(), getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        String title = ("0000000" + item.getTitle()).substring(item.getTitle().length());
                                                        BluetoothConnection.getDefaultInstance().commandConfigure(
                                                                String.format(Locale.getDefault(), "%015d", App.get().getModification().getId()),
                                                                title,
                                                                item.getParam1(), item.getParam2(), item.getParam3(),
                                                                content);
                                                        BluetoothConnection.setLastSettingsItem(item);
                                                        BluetoothConnection.setLastSettingsItemContent(content);
                                                    }
                                                }
                                            });
                                        } else {
                                            hideProgressDialog();
                                            CustomDialog cDialog = new CustomDialog();
                                            cDialog.setMessage("Error occurred");
                                            cDialog.setLeftButton(context.getString(R.string.ok), null);
                                            cDialog.setRightButton(getString(R.string.contact_support), new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    EventBus.getDefault().post(new TransactionHomeFragment(3));
                                                }
                                            });
                                            cDialog.show(getFragmentManager(), "dialog");
                                        }
                                    }
                                });
                    }
                });
                dialog.setRightButton(context.getString(R.string.cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dialog.show(getFragmentManager(), "dialog");
            }
        });
        return view;
    }

    private void getMarks() {
        RestHelper.getInstance().selectCarInterface.getMarks().enqueue(new Callback<Car>() {
            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                items = response.body().getData();
                listView.setAdapter(new SelectCarAdapter(context, response.body().getData()));
            }

            @Override
            public void onFailure(Call<Car> call, Throwable t) {
                Toast.makeText(getContext(), getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getModels(int id) {
        RestHelper.getInstance().selectCarInterface.getModels(id).enqueue(new Callback<Car>() {
            @Override
            public void onResponse(Call<Car> call, Response<Car> response) {
                items = response.body().getData();
                listView.setAdapter(new SelectCarAdapter(context, response.body().getData()));
            }

            @Override
            public void onFailure(Call<Car> call, Throwable t) {
                Toast.makeText(getContext(), getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getModifications(int id) {
        RestHelper.getInstance().selectCarInterface.getModifications(id).enqueue(new Callback<Modification>() {
            @Override
            public void onResponse(Call<Modification> call, Response<Modification> response) {
                modifications = response.body().getData();
                modificationsAdapter = new SelectionAdapter(context, response.body().getTitles(), -1);
                listView.setAdapter(modificationsAdapter);
            }

            @Override
            public void onFailure(Call<Modification> call, Throwable t) {
                Toast.makeText(getContext(), getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Subscribe
    public void onEvent(EventBluetoothStatusReceived event) {
        if (getActivity() != null)
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideProgressDialog();
                    App.get().setDemo(false);
                    App.get().setSelectionStage(0);
                    App.get().setSelectionMode(false);
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    EventBus.getDefault().post(new TransactionFragment(new DashboardFragment(), ""));
                }
            });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (App.get().getSelectionStage()) {
            case 0:
                App.get().setMark(items.get(position).getTitle(), items.get(position).getId());
                App.get().setSelectionStage(1);
                EventBus.getDefault().post(new TransactionFragment(new SelectCarFragment(), ""));
                break;
            case 1:
                App.get().setModel(items.get(position).getTitle(), items.get(position).getId());
                App.get().setSelectionStage(2);
                EventBus.getDefault().post(new TransactionFragment(new SelectCarFragment(), ""));
                break;
            case 2:
                if (bottomView.getVisibility() == View.GONE) Utils.showSlideInUp(bottomView);
                description.setText(
                        App.get().getMark() + " " + App.get().getModel() + "\n"
                                + modifications.get(position).getTitle() + "\n"
                                + modifications.get(position).getHp() + "hp " + modifications.get(position).getNm() + "nm");
                modificationsAdapter.setSelectedPosition(position);
                modificationsAdapter.notifyDataSetChanged();
                break;
        }
    }
}
