package it.rschip.rschip.ui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.rschip.rschip.R;
import it.rschip.rschip.bluetooth.BluetoothConnection;
import it.rschip.rschip.bluetooth.DeviceStatus;
import it.rschip.rschip.utils.LoggingHelper;
import it.rschip.rschip.utils.RestHelper;
import it.rschip.rschip.utils.ToolbarHelper;
import it.rschip.rschip.utils.Utils;
import it.rschip.rschip.views.CustomDialog;

/**
 * Created by Dan on 25.05.2016.
 */
public class ContactUsFragment extends BaseFragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_contact_us, null);
        view.findViewById(R.id.email_us).setOnClickListener(this);
        view.findViewById(R.id.send_log).setOnClickListener(this);
        view.findViewById(R.id.visit_site).setOnClickListener(this);

        if (BluetoothConnection.getDefaultInstance() != null && BluetoothConnection.getDefaultInstance().getDeviceStatus() != null) {
            DeviceStatus status = BluetoothConnection.getDefaultInstance().getDeviceStatus();
            ((TextView) view.findViewById(R.id.firmware_version))
                    .setText(String.format(getString(R.string.firmware_version), status.getSoftwareVersion(), status.getSoftwareSubversion()));
            ((TextView) view.findViewById(R.id.program_version))
                    .setText(String.format(getString(R.string.program_version), status.getProgramVersion()));
            ((TextView) view.findViewById(R.id.rschip_serial))
                    .setText(String.format(getString(R.string.rschip_serial), BluetoothConnection.getDefaultInstance().getDeviceId()));
            ((TextView) view.findViewById(R.id.activatoin_mode))
                    .setText(String.format(getString(R.string.activation_mode) + " %1$s", status.getFWMode()));
        }
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.email_us:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + getString(R.string.email)));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subject));
                emailIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.title_email) + "\n\n" + Utils.getDeviceName() + " " + Utils.getAndroidVersion());
                try {
                    startActivity(Intent.createChooser(emailIntent, getString(R.string.email_us)));
                } catch (android.content.ActivityNotFoundException ex) {
                }
                break;
            case R.id.send_log:
                if (!LoggingHelper.getInstance().exists()) {
                    new CustomDialog().setMessage(getString(R.string.loggin_nothing_to_upload)).
                            setLeftButton(getString(android.R.string.ok), null).show(getChildFragmentManager(), "");
                } else {
                    showProgressDialog("");
                    RestHelper.getInstance().sendLogField(new RestHelper.CallbackStatus() {
                        @Override
                        public void result(boolean status) {
                            hideProgressDialog();
                            if (status) {
                                new CustomDialog().setMessage(getString(R.string.log_successfully_send)).setLeftButton(getString(android.R.string.ok), null).show(getChildFragmentManager(), "");
                                LoggingHelper.getInstance().deleteLogFile();
                            } else
                                new CustomDialog().setMessage(getString(R.string.log_sending_failed)).setLeftButton(getString(android.R.string.ok), null).show(getChildFragmentManager(), "");
                        }
                    });
                }
                break;
            case R.id.visit_site:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.site)));
                startActivity(browserIntent);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarHelper.getInstance().setTitle(getString(R.string.contact_us));
    }
}