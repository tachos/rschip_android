package it.rschip.rschip.ui.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import it.rschip.rschip.App;
import it.rschip.rschip.R;
import it.rschip.rschip.adapters.SelectionAdapter;
import it.rschip.rschip.bluetooth.BluetoothConnection;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandSetFWMode;
import it.rschip.rschip.bluetooth.events.EventBluetoothStatusReceived;
import it.rschip.rschip.utils.ToolbarHelper;
import it.rschip.rschip.utils.TransactionFragment;
import it.rschip.rschip.views.InfoDialogFragment;

/**
 * Created by Dan on 26.05.2016.
 */
public class ServiceToolsFragment extends BaseFragment {

    @SuppressLint("DefaultLocale")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_service_tools, null);
        if (BluetoothConnection.getDefaultInstance() != null)
            ((TextView) view.findViewById(R.id.tv_reconf_count)).setText(
                    String.format(getString(R.string.tunefiles_left), BluetoothConnection.getDefaultInstance().getTunefilesLeft()));
        view.findViewById(R.id.restore_rschip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.get().isDemo()) {
                    showProgressDialog(null);
                    BluetoothConnection.getDefaultInstance().commandReconfigure();
                    try {
                        if (Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareVersion()) <= 9
                                && Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareSubversion()) < 40) {
                            if (getActivity() != null)
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (BluetoothConnection.getDefaultInstance() != null
                                                        && BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_CONNECTED)
                                                    hideProgressDialog();
                                            }
                                        }, 30000);
                                    }
                                });
                            showProgressDialog(getString(R.string.calibration_waiting));
                        }
                    } catch (Exception ignored) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                            }
                        });
                    }
                }
            }
        });
        view.findViewById(R.id.disconnect_rschip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BluetoothConnection.getDefaultInstance() != null)
                    BluetoothConnection.getDefaultInstance().cancel();
                BluetoothConnection.reset();
                App.get().setDemo(false);
                EventBus.getDefault().post(new TransactionFragment(new HomeFragment(), ""));
            }
        });
        view.findViewById(R.id.info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InfoDialogFragment().show(getChildFragmentManager(), "info");
            }
        });
        ListView listView = (ListView) view.findViewById(R.id.listView);
        final SelectionAdapter adapter = new SelectionAdapter(view.getContext(), getResources().getStringArray(R.array.items_activation_mode), App.get().getPositionActivationMode());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        App.get().setActivationMode(position);
                        adapter.setSelectedPosition(position);
                        adapter.notifyDataSetChanged();

                        if (!App.get().isDemo()) {
                            showProgressDialog(null);
                            String fwMode;
                            if (position == 0) fwMode = BluetoothCommandSetFWMode.MODE_A;
                            else if (position == 1) fwMode = BluetoothCommandSetFWMode.MODE_B;
                            else fwMode = BluetoothCommandSetFWMode.MODE_X;
                            BluetoothConnection.getDefaultInstance().commandSetFWMode(fwMode);
                            try {
                                if (Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareVersion()) <= 9
                                        && Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareSubversion()) < 40) {
                                    if (getActivity() != null)
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (BluetoothConnection.getDefaultInstance() != null
                                                                && BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_CONNECTED)
                                                            hideProgressDialog();
                                                    }
                                                }, 30000);
                                            }
                                        });
                                    showProgressDialog(getString(R.string.calibration_waiting));
                                }
                            } catch (Exception ignored) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        hideProgressDialog();
                                    }
                                });
                            }
                        }
                    }
                }
        );
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (BluetoothConnection.getDefaultInstance() != null && !App.get().isDemo()) {
            if (BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_CONNECTED || BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_DISCONNECTED)
                hideProgressDialog();
        }
    }

    @Override
    @Subscribe
    public void onEvent(EventBluetoothStatusReceived event) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToolbarHelper.getInstance().setStatusConnected();
                if (Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareVersion()) <= 9
                        && Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareSubversion()) < 40)
                    ;
                else hideProgressDialog();
            }
        });
    }
}
