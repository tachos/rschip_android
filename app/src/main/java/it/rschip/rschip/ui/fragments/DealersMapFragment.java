package it.rschip.rschip.ui.fragments;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.List;

import it.rschip.rschip.R;
import it.rschip.rschip.models.Dealer;
import it.rschip.rschip.utils.RestHelper;
import it.rschip.rschip.utils.SendDealers;
import it.rschip.rschip.utils.TransactionFragment;
import it.rschip.rschip.utils.Utils;

/**
 * Created by Dan on 23.05.2016.
 */
public class DealersMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener, View.OnClickListener {

    private static View view;
    private GoogleMap mMap;
    private View mSnack;
    private TextView mName;
    private TextView mDescription;
    private BitmapDescriptor markerDealer;
    private HashMap<Marker, Dealer> mDealers;
    private Dealer selectedDealer;

    private static final int LOCATION_FINE_PERMISSION_CODE = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_dealers_map, null);
            if (Utils.checkPlayServices(getActivity())) {
                ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
                mSnack = view.findViewById(R.id.snack);
                mName = (TextView) view.findViewById(R.id.name);
                mDescription = (TextView) view.findViewById(R.id.description);
                ((ImageView) view.findViewById(R.id.imageView)).setColorFilter(ContextCompat.getColor(getContext(), R.color.gray));
                markerDealer = BitmapDescriptorFactory.fromResource(R.drawable.marker);
            }
        } catch (InflateException e) {
            Log.e("DealersMapFragment", e.toString());
        }
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(this);
        mSnack.setOnClickListener(this);

        if (RestHelper.getInstance().responseDealers != null) {
            addMarkerDealers(RestHelper.getInstance().responseDealers.body());
        }

        addLocationListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onEvent(SendDealers event) {
        if (mMap != null) {
            addMarkerDealers(event.getDealers());
        }
    }

    private void addMarkerDealers(List<Dealer> dealers) {
        mDealers = new HashMap<>();
        if(dealers != null) {
            for (Dealer dealer : dealers) {
                Marker add = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(dealer.getLat(), dealer.getLon()))
                        .icon(markerDealer));
                mDealers.put(add, dealer);
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (mSnack.getVisibility() == View.GONE) Utils.showSlideInUp(mSnack);
        selectedDealer = mDealers.get(marker);
        mName.setText(selectedDealer.getName());
        mDescription.setText(selectedDealer.getShort_description());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CameraPosition camPos = new CameraPosition.Builder()
                        .target(new LatLng(selectedDealer.getLat(), selectedDealer.getLon()))
                        .zoom(10)
                        .build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
            }
        }, 300);
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (mSnack.getVisibility() == View.VISIBLE) Utils.hideSlideOutDown(mSnack);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.snack) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("details", selectedDealer);
            DealersDetailsFragment detailsFragment = new DealersDetailsFragment();
            detailsFragment.setArguments(bundle);
            EventBus.getDefault().post(new TransactionFragment(detailsFragment, getString(R.string.Details)));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_FINE_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    addLocationListener();
                }
            }
        }
    }

    private void addLocationListener() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, LOCATION_FINE_PERMISSION_CODE);
        } else {
            mMap.setMyLocationEnabled(true);

            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    CameraPosition camPos = new CameraPosition.Builder()
                            .target(new LatLng(location.getLatitude(), location.getLongitude()))
                            .zoom(10)
                            .build();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            }, null);
        }
    }
}
