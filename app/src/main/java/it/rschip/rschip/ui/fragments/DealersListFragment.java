package it.rschip.rschip.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import it.rschip.rschip.R;
import it.rschip.rschip.adapters.DealersAdapter;
import it.rschip.rschip.models.Dealer;
import it.rschip.rschip.utils.RestHelper;
import it.rschip.rschip.utils.SendDealers;
import it.rschip.rschip.utils.TransactionFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dan on 23.05.2016.
 */
public class DealersListFragment extends Fragment {

    private ListView listView;
    private List<Dealer> items;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        listView = new ListView(inflater.getContext());
        listView.setOverScrollMode(View.OVER_SCROLL_NEVER);
        listView.setDividerHeight(0);
        listView.setSelector(android.R.color.transparent);

        RestHelper.getInstance().getDealers(new Callback<List<Dealer>>() {
            @Override
            public void onResponse(Call<List<Dealer>> call, Response<List<Dealer>> response) {
                if(response.body() != null && response.body().size() > 0) {
                    items = response.body();
                    EventBus.getDefault().post(new SendDealers(items));
                    listView.setAdapter(new DealersAdapter(inflater.getContext(), items));
                }
            }

            @Override
            public void onFailure(Call<List<Dealer>> call, Throwable t) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("details", items.get(position));
                DealersDetailsFragment detailsFragment = new DealersDetailsFragment();
                detailsFragment.setArguments(bundle);
                EventBus.getDefault().post(new TransactionFragment(detailsFragment, getString(R.string.Details)));
            }
        });

        return listView;
    }
}
