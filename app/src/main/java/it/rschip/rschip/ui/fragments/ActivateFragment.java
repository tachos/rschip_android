package it.rschip.rschip.ui.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.widget.Button;
import android.widget.ListView;

import com.google.gson.JsonObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;

import it.rschip.rschip.App;
import it.rschip.rschip.R;
import it.rschip.rschip.adapters.RsChipAdapter;
import it.rschip.rschip.bluetooth.BluetoothConnection;
import it.rschip.rschip.bluetooth.BluetoothConnectionInterface;
import it.rschip.rschip.bluetooth.events.EventBluetoothStatusReceived;
import it.rschip.rschip.bluetooth.events.errors.EventBluetoothErrorConnectionLost;
import it.rschip.rschip.bluetooth.events.errors.EventBluetoothErrorTimeout;
import it.rschip.rschip.models.ModificationItem;
import it.rschip.rschip.models.Settings;
import it.rschip.rschip.utils.RestHelper;
import it.rschip.rschip.utils.ToolbarHelper;
import it.rschip.rschip.utils.TransactionFragment;
import it.rschip.rschip.utils.TransactionHomeFragment;
import it.rschip.rschip.utils.Utils;
import it.rschip.rschip.views.CustomDialog;

/**
 * Created by Dan on 20.05.2016.
 */
public class ActivateFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final int REQUEST_ENABLE_BT = 613;
    private static final int BLUETOOTH_CONNECT_PERMISSION_CODE = 0;
    private static final int BLUETOOTH_SCAN_PERMISSION_CODE = 1;
    private static final int BLUETOOTH_CONNECT_ENABLE_PERMISSION_CODE = 2;
    BluetoothAdapter mBluetoothAdapter;
    SwipeRefreshLayout swipeRefresh;
    ArrayList<BluetoothDevice> items;
    RsChipAdapter adapter;
    Button bottomButton;
    View bottomView;
    Intent broadcastRecieverIntent;
    Boolean actionStart;

    private BroadcastReceiver mReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_activate, null);
        App.get().setSelectionStage(0);

        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        ListView listView = (ListView) view.findViewById(R.id.listView);
        bottomView = view.findViewById(R.id.bottomView);
        bottomButton = (Button) view.findViewById(R.id.bottomButton);
        items = new ArrayList<>();
        adapter = new RsChipAdapter(view.getContext(), items);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((parent, view1, position, id) -> {
            if (bottomView.getVisibility() == View.GONE) Utils.showSlideInUp(bottomView);
            adapter.setSelectedPosition(position);
            adapter.notifyDataSetChanged();
        });

        bottomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelDiscovery();
                swipeRefresh.setRefreshing(false);
                final CustomDialog dialog = new CustomDialog();
                dialog.setTitle(getString(R.string.authTitleLabel))
                        .setPasswordView(getString(R.string.noticeLabel), new CustomDialog.OnClickConfirmListener() {
                            @Override
                            public void OnClickConfirm(String password) {
                                final BluetoothConnection bluetoothConnection;
                                if (BluetoothConnection.getDefaultInstance() != null) {
                                    BluetoothConnection.getDefaultInstance().cancel();
                                }
                                bluetoothConnection = new BluetoothConnection(adapter.getSelectedItem(), password);


                                showProgressDialog(null);
                                final Object listener = new Object() {
                                    @Subscribe
                                    public void onEvent(EventBluetoothErrorConnectionLost event) {
                                        hideProgressDialog();
                                        EventBus.getDefault().unregister(this);
                                    }

                                    @Subscribe
                                    public void onEvent(EventBluetoothErrorTimeout event) {
                                        hideProgressDialog();
                                        EventBus.getDefault().unregister(this);
                                    }
                                };
                                EventBus.getDefault().register(listener);
                                bluetoothConnection.connect(new BluetoothConnectionInterface() {
                                    @Override
                                    public void connectionSuccess() {
                                        Log.d("test", "connectionSuccess");
                                        EventBus.getDefault().unregister(listener);
                                        checkNeedUpgrade(dialog);
                                    }

                                    @Override
                                    public void connectionFailed() {
                                        hideProgressDialog();
                                        EventBus.getDefault().unregister(listener);
                                        dialog.setDescriptionText(getString(R.string.error_unable_to_connect), true);
                                        bluetoothConnection.cancel();
                                    }

                                    @Override
                                    public void wrongPassword() {
                                        hideProgressDialog();
                                        EventBus.getDefault().unregister(listener);
                                        dialog.setDescriptionText(getString(R.string.error_wrong_password), true);
                                    }
                                });
                            }
                        })
                        .setRightButton(getString(R.string.connectButton), null)
                        .setLeftButton(getString(R.string.cancelButton), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (BluetoothConnection.getDefaultInstance() != null) {
                                    BluetoothConnection.getDefaultInstance().cancel();
                                }
                            }
                        })
                        .show(getActivity().getSupportFragmentManager(), "CustomDialog");
            }
        });
        swipeRefresh.setOnRefreshListener(this);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            mBluetoothAdapter = ((BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
        else mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                broadcastRecieverIntent = intent;
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[] { Manifest.permission.BLUETOOTH_CONNECT }, BLUETOOTH_CONNECT_PERMISSION_CODE);
                        } else {
                            addBluetoothDeviceToList(intent);
                        }
                    } else {
                        addBluetoothDeviceToList(intent);
                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        getActivity().registerReceiver(mReceiver, filter);

        startRefreshing();

        return view;
    }

    private void checkNeedUpgrade(final CustomDialog dialog) {
        RestHelper.getInstance().checkNeedUpgrade(BluetoothConnection.getDefaultInstance().getDeviceId(), new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String s) {
                switch (s) {
                    case "":
                        authSuccess(dialog);
                        break;
                    case "error":
                        hideProgressDialog();
                        break;
                    default:
                        dialog.dismiss();
                        hideProgressDialog();
                        NeedUpgrade needUpgrade = new NeedUpgrade();
                        Bundle bundle = new Bundle();
                        bundle.putString(NeedUpgrade.URL, s);
                        needUpgrade.setArguments(bundle);
                        EventBus.getDefault().post(new TransactionFragment(needUpgrade, ""));
                        break;
                }
            }
        });
    }

    @SuppressLint("DefaultLocale")
    private void authSuccess(final CustomDialog dialog) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    showProgressDialog(null);
                    BluetoothConnection.getDefaultInstance().setTunefilesLeft(
                            RestHelper.getInstance().selectCarInterface.getReconfCount(BluetoothConnection.getDefaultInstance().getDeviceId()).execute().body().getReconf_count());

                    final CustomDialog connectDialog = new CustomDialog();
                    String message = String.format(
                            getString(R.string.additional_tunefiles_may_require),
                            BluetoothConnection.getDefaultInstance().getTunefilesLeft());

                    if (!BluetoothConnection.getDefaultInstance().getDeviceStatus().getModification().equals("000000000000000")) {
                        JsonObject response = RestHelper.getInstance().selectCarInterface.getModification(BluetoothConnection.getDefaultInstance().getDeviceStatus().getModification()).execute().body();

                        ModificationItem modification = new ModificationItem();
                        modification.setHp(response.get("hp").getAsInt());
                        modification.setNm(response.get("nm").getAsInt());
                        modification.setId(response.get("id").getAsInt());
                        modification.setTitle(response.get("title").getAsString());
                        App.get().setModification(modification);
                        App.get().setModel(response.get("model").getAsJsonObject().get("title").getAsString(),
                                response.get("model").getAsJsonObject().get("id").getAsInt());
                        App.get().setMark(response.get("model").getAsJsonObject().get("brand").getAsJsonObject().get("title").getAsString(),
                                response.get("model").getAsJsonObject().get("brand").getAsJsonObject().get("id").getAsInt());

                        connectDialog.setLeftButton(getString(R.string.use_curr_car), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showProgressDialog(null);
                                connectDialog.dismiss();
                                RestHelper.getInstance().getSettings(App.get().getModification().getId(), new RestHelper.CallbackFile() {
                                    @Override
                                    public void result(String content, Settings.Item item) {
                                        hideProgressDialog();
                                        if (content.isEmpty()) {
                                            dialog.setDescriptionText(getString(R.string.error_unable_to_connect), true);
                                            BluetoothConnection.getDefaultInstance().cancel();
                                        } else {
                                            dialog.dismiss();
                                            BluetoothConnection.setLastSettingsItem(item);
                                            BluetoothConnection.setLastSettingsItemContent(content);
                                            App.get().setDemo(false);
                                            App.get().setSelectionStage(0);
                                            App.get().setSelectionMode(false);
                                            BluetoothConnection.setLastMac(BluetoothConnection.getDefaultInstance().getMac());
                                            BluetoothConnection.setLastName(BluetoothConnection.getDefaultInstance().getName());
                                            BluetoothConnection.setLastPassword(BluetoothConnection.getDefaultInstance().getPassword());
                                            getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            EventBus.getDefault().post(new TransactionFragment(new DashboardFragment(), ""));
                                        }
                                    }
                                });

                            }
                        });
                        message += String.format(getString(R.string.activate_current_car),
                                App.get().getMark(),
                                App.get().getModel(),
                                App.get().getModification().getTitle(),
                                App.get().getModification().getHp(),
                                App.get().getModification().getNm());
                    }

                    connectDialog.setMessage(message);

                    if (BluetoothConnection.getDefaultInstance().getTunefilesLeft() > 0)
                        connectDialog.setRightButton(getString(R.string.select_new_car), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                connectDialog.dismiss();
                                dialog.dismiss();
                                hideProgressDialog();
                                EventBus.getDefault().post(new TransactionFragment(new SelectCarFragment(), getString(R.string.select_car)));
                            }
                        });
                    else
                        connectDialog.setRightButton(getString(R.string.contact_support), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                connectDialog.dismiss();
                                dialog.dismiss();
                                BluetoothConnection.getDefaultInstance().cancel();
                                EventBus.getDefault().post(new TransactionHomeFragment(3));
                            }
                        });
                    connectDialog.show(getFragmentManager(), "dialog");

                    hideProgressDialog();
                } catch (IOException e) {
                    e.printStackTrace();
                    hideProgressDialog();
                    dialog.setDescriptionText(getString(R.string.error_unable_to_connect), true);
                    BluetoothConnection.getDefaultInstance().cancel();
                }


            }
        }).start();

/*        if (getActivity() != null)
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                }
            });*/
    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarHelper.getInstance().showBack(getString(R.string.selectRSCHIP));
    }

    @Override
    public void onRefresh() {
        if (!mBluetoothAdapter.isEnabled()) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] { Manifest.permission.BLUETOOTH_CONNECT }, BLUETOOTH_CONNECT_ENABLE_PERMISSION_CODE);
                } else {
                    requestBluetooth();
                }
            } else {
                requestBluetooth();
            }
        }
        items.clear();
        if (BluetoothConnection.getDefaultInstance() != null)
            BluetoothConnection.getDefaultInstance().cancel();
        if (bottomView.getVisibility() == View.VISIBLE) Utils.hideSlideOutDown(bottomView);
        cancelDiscovery();
        startDiscovery();
        adapter.setSelectedPosition(-1);
        adapter.notifyDataSetChanged();
        stopRefreshing();
    }

    @Override
    public void onEvent(EventBluetoothStatusReceived event) {
    }

    @Override
    public void onPause() {
        if (mBluetoothAdapter != null) cancelDiscovery();
        swipeRefresh.setRefreshing(false);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (mReceiver != null)
            getActivity().unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ENABLE_BT: {
                if (resultCode != Activity.RESULT_CANCELED) {
                } else {
                    Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(discoverableIntent, REQUEST_ENABLE_BT);
                }
                break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case BLUETOOTH_CONNECT_ENABLE_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startDiscovery();
                }
            }
            case BLUETOOTH_SCAN_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (actionStart) {
                        startDiscovery();
                    } else {
                        cancelDiscovery();
                    }
                }
            }
            case BLUETOOTH_CONNECT_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (broadcastRecieverIntent != null) {
                        addBluetoothDeviceToList(broadcastRecieverIntent);
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void addBluetoothDeviceToList(Intent intent) {
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        boolean isDeviceHaveAppropriateName = device.getName() != null
                && device.getName().toLowerCase().contains("rschip");
        boolean isDeviceTypeIsClassic = true;
        boolean isContains = adapter.contains(device);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            isDeviceTypeIsClassic = device.getType() == BluetoothDevice.DEVICE_TYPE_CLASSIC;
        }
        if (isDeviceHaveAppropriateName && isDeviceTypeIsClassic && !isContains) {
            items.add(device);
            adapter.notifyDataSetChanged();
        }
    }

    private void startDiscovery() {
        actionStart = true;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] { Manifest.permission.BLUETOOTH_SCAN, Manifest.permission.ACCESS_FINE_LOCATION }, BLUETOOTH_SCAN_PERMISSION_CODE);
            } else {
                mBluetoothAdapter.startDiscovery();
            }
        } else {
            mBluetoothAdapter.startDiscovery();
        }
    }

    private void cancelDiscovery() {
        actionStart = false;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] { Manifest.permission.BLUETOOTH_SCAN, Manifest.permission.ACCESS_FINE_LOCATION }, BLUETOOTH_SCAN_PERMISSION_CODE);
            } else {
                mBluetoothAdapter.cancelDiscovery();
            }
        } else {
            mBluetoothAdapter.cancelDiscovery();
        }
    }

    private void requestBluetooth() {
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        startActivityForResult(discoverableIntent, REQUEST_ENABLE_BT);
    }

    private void startRefreshing() {
        swipeRefresh.post(() -> {
            swipeRefresh.setRefreshing(true);
            onRefresh();
        });
    }

    private void stopRefreshing() {
        swipeRefresh.postDelayed(() -> swipeRefresh.setRefreshing(false), 13000);
    }
}
