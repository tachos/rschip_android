package it.rschip.rschip.ui.fragments;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import info.hoang8f.android.segmented.SegmentedGroup;
import it.rschip.rschip.R;
import it.rschip.rschip.adapters.DealersPagerAdapter;
import it.rschip.rschip.utils.ToolbarHelper;

/**
 * Created by Dan on 23.05.2016.
 */
public class DealersFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.fragment_dealers, null);
        final SegmentedGroup segmentedGroup = (SegmentedGroup) view.findViewById(R.id.segmented);
        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        viewPager.setAdapter(new DealersPagerAdapter(getChildFragmentManager()));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                segmentedGroup.check(position == 0 ? R.id.buttonList : R.id.buttonMap);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        segmentedGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.buttonList:
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.buttonMap:
                        viewPager.setCurrentItem(1);
                        break;
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarHelper.getInstance().showBack(getString(R.string.Find_a_dealer));
    }
}
