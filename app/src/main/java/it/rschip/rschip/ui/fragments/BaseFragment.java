package it.rschip.rschip.ui.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import it.rschip.rschip.R;
import it.rschip.rschip.bluetooth.events.EventBluetoothStatusReceived;
import it.rschip.rschip.bluetooth.events.calibrating.EventBluetoothCalibratingCompleted;
import it.rschip.rschip.bluetooth.events.calibrating.EventBluetoothCalibratingInterrupted;
import it.rschip.rschip.bluetooth.events.calibrating.EventBluetoothCalibratingProgress;
import it.rschip.rschip.bluetooth.events.errors.EventBluetoothError;
import it.rschip.rschip.utils.ToolbarHelper;
import it.rschip.rschip.utils.Utils;

/**
 * Created by Dan on 17.05.2016.
 */
public abstract class BaseFragment extends Fragment {
    private static Dialog progressDialog;
    private long nextStatusMessageAvailable = 0;

    private long lastProgressShowed = 0;

    public void showProgressDialog(final String progress) {
        lastProgressShowed = 0;
        if (getActivity() == null) return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog == null || !progressDialog.isShowing()) {
                    progressDialog = new Dialog(getActivity(), R.style.AppTheme);
                    progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    progressDialog.setContentView(R.layout.progress_dialog);
                    progressDialog.setCancelable(false);
                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#aa000000")));
                    progressDialog.show();
                }

                if (progress != null && !progress.isEmpty()) {
                    progressDialog.findViewById(R.id.rl_progress_container).setVisibility(View.VISIBLE);
                    try {
                        ((ProgressBar) progressDialog.findViewById(R.id.pb_calibrating)).setProgress(Integer.valueOf(progress));
                        ((TextView) progressDialog.findViewById(R.id.tv_progress_description)).setText(getString(R.string.calibrating, progress));
                    } catch (Exception ignored) {
                        ((ProgressBar) progressDialog.findViewById(R.id.pb_calibrating)).setProgress(0);
                        ((TextView) progressDialog.findViewById(R.id.tv_progress_description)).setText(progress);
                        lastProgressShowed = System.currentTimeMillis();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (lastProgressShowed != 0 && lastProgressShowed <= System.currentTimeMillis())
                                    hideProgressDialog();
                            }
                        }, 30000);
                    }
                } else {
                    progressDialog.findViewById(R.id.rl_progress_container).setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void onEvent(EventBluetoothCalibratingProgress event) {
        showProgressDialog(String.valueOf(event.getProgress()));
    }

    @Subscribe
    public void onEvent(EventBluetoothCalibratingCompleted event) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                showStatusCalibrationComplete();
            }
        });
    }

    @Subscribe
    public void onEvent(EventBluetoothCalibratingInterrupted event) {
        hideProgressDialog();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showStatusCalibrationInterrupted();
            }
        });
    }

    public void hideProgressDialog() {
        if (progressDialog != null && getActivity() != null)
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            });
    }

    public void showStatusCalibrationInterrupted() {
        showStatus(ToolbarHelper.getInstance().getChipStatus(), getString(R.string.calibrationInterrupted));
    }

    public void showStatusCalibrationComplete() {
        showStatus(ToolbarHelper.getInstance().getChipStatus(), getString(R.string.calibrationComplete));
    }

    public void showStatusRenewed() {
        showStatus(ToolbarHelper.getInstance().getChipStatus(), getString(R.string.statusRenewed));
    }

    public void showStatusModeSet() {
        showStatus(ToolbarHelper.getInstance().getChipStatus(), getString(R.string.modeSet));
    }

    public void showStatusActivationModeSet() {
        showStatus(ToolbarHelper.getInstance().getChipStatus(), getString(R.string.activationModeSet));
    }

    public void showStatusRestoreSucceed() {
        showStatus(ToolbarHelper.getInstance().getChipStatus(), getString(R.string.restoreSucceed));
    }

    public void showStatusConnectingProblem() {
        showStatus(ToolbarHelper.getInstance().getChipStatus(), getString(R.string.error_unable_to_connect));
    }

    private void showStatus(final View view, final String text) {
        if (nextStatusMessageAvailable < System.currentTimeMillis())
            nextStatusMessageAvailable = System.currentTimeMillis();
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                ToolbarHelper.getInstance().getChipStatus().setText(text);
                Utils.showSlideInDown(view);
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Utils.hideSlideOutUp(view);
                    }
                }, 2500);
            }
        }, nextStatusMessageAvailable - System.currentTimeMillis());
        nextStatusMessageAvailable += 2500 + 300 + 300;
    }


    @Subscribe
    public void onEvent(EventBluetoothStatusReceived event) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToolbarHelper.getInstance().setStatusConnected();
                hideProgressDialog();
                showStatusModeSet();
            }
        });
    }

    @Subscribe
    public void onEvent(EventBluetoothError event) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                ToolbarHelper.getInstance().setStatusDisconnected();
                showStatusConnectingProblem();
            }
        });
    }
}
