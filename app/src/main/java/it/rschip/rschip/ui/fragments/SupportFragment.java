package it.rschip.rschip.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;

import it.rschip.rschip.R;
import it.rschip.rschip.utils.ToolbarHelper;
import it.rschip.rschip.utils.TransactionFragment;

/**
 * Created by Dan on 24.05.2016.
 */
public class SupportFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_support, null);
        view.findViewById(R.id.contact_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new TransactionFragment(new ContactUsFragment(), getString(R.string.contact_us)));
            }
        });
        view.findViewById(R.id.language).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new TransactionFragment(new LanguageFragment(), getString(R.string.language)));
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarHelper.getInstance().showBack(getString(R.string.Support));
    }
}
