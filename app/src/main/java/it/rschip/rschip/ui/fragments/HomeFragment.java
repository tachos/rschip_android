package it.rschip.rschip.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import org.greenrobot.eventbus.EventBus;

import it.rschip.rschip.App;
import it.rschip.rschip.R;
import it.rschip.rschip.adapters.HomeAdapter;
import it.rschip.rschip.bluetooth.BluetoothConnection;
import it.rschip.rschip.utils.TransactionFragment;
import it.rschip.rschip.utils.TransactionHomeFragment;
import it.rschip.rschip.views.PullToZoomListView;

/**
 * Created by Dan on 16.05.2016.
 */
public class HomeFragment extends BaseFragment {

    PullToZoomListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_home, null);
        listView = (PullToZoomListView) view.findViewById(R.id.listView);
        final HomeAdapter adapter = new HomeAdapter(view.getContext());
        listView.setAdapter(adapter);
        final View buttonCancel = view.findViewById(R.id.bt_home_cancel);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    if (App.get().isDemo()) {
                        App.get().setDemo(false);
                        adapter.notifyDataSetChanged();
                        buttonCancel.setVisibility(View.INVISIBLE);
                        return;
                    } else {
                        App.get().setDemo(true);
                    }
                }
                if (id == 2 && !BluetoothConnection.getLastMac().isEmpty()) {
                    App.get().setSelectionStage(0);
                    EventBus.getDefault().post(new TransactionFragment(new SelectCarFragment(), getString(R.string.select_car)));
                } else
                    EventBus.getDefault().post(new TransactionHomeFragment((int) id));
            }
        });
        listView.setSoundEffectsEnabled(false);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new TransactionHomeFragment(0));
            }
        });
        if (BluetoothConnection.getLastMac().isEmpty() && !App.get().isDemo())
            buttonCancel.setVisibility(View.INVISIBLE);
        else buttonCancel.setVisibility(View.VISIBLE);

        return view;
    }
}
