package it.rschip.rschip.ui.fragments;

import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import it.rschip.rschip.App;
import it.rschip.rschip.R;
import it.rschip.rschip.bluetooth.BluetoothConnection;
import it.rschip.rschip.bluetooth.BluetoothConnectionInterface;
import it.rschip.rschip.bluetooth.DeviceStatus;
import it.rschip.rschip.bluetooth.events.EventBluetoothStatusReceived;
import it.rschip.rschip.bluetooth.events.errors.EventBluetoothError;
import it.rschip.rschip.models.ReconfCount;
import it.rschip.rschip.utils.RestHelper;
import it.rschip.rschip.utils.SlidingMenuHelper;
import it.rschip.rschip.utils.ToolbarHelper;
import it.rschip.rschip.utils.TransactionFragment;
import it.rschip.rschip.views.Dashboard.DashedCircularProgress;
import it.rschip.rschip.views.Dashboard.SlideSwitch;
import it.rschip.rschip.views.InfoDialogFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dan on 25.05.2016.
 */
public class DashboardFragment extends BaseFragment implements View.OnClickListener, SlideSwitch.SlideListener, DashedCircularProgress.OnAnimChangeListener {

    DashedCircularProgress dashedCircularProgress;
    SlideSwitch slideSwitch;
    TextView slide_off;
    TextView nameMode;
    TextView hp;
    TextView nm;

    View btSpp, btSp, btEco;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.fragment_dashboard, null);
        dashedCircularProgress = (DashedCircularProgress) view.findViewById(R.id.dashboard);
        dashedCircularProgress.setAnimChangeListener(this);
        slideSwitch = (SlideSwitch) view.findViewById(R.id.slideSwitch);
        nameMode = (TextView) view.findViewById(R.id.name_mode);
        hp = (TextView) view.findViewById(R.id.hp);
        nm = (TextView) view.findViewById(R.id.nm);
        slide_off = (TextView) view.findViewById(R.id.slide_off);
        slideSwitch.setSlideListener(this);

        btSpp = view.findViewById(R.id.button_sportp);
        btSp = view.findViewById(R.id.button_sport);
        btEco = view.findViewById(R.id.button_eco);
        view.findViewById(R.id.info).setOnClickListener(this);
        btSpp.setOnClickListener(this);
        btSp.setOnClickListener(this);
        btEco.setOnClickListener(this);

        //slideSwitch.setState(false);
        dashedCircularProgress.off();
        ToolbarHelper.getInstance().setStatusDisconnected();

        if (App.get().isDemo()) {
            SlidingMenuHelper.getInstance().setDemo();
            slideSwitch.setState(true);
            ToolbarHelper.getInstance().setStatusConnected();
            dashedCircularProgress.setSportPlus();
            nameMode.setText(getString(R.string.sport_plus));
            btSpp.setActivated(true);
        } else {
            SlidingMenuHelper.getInstance().setActivate();
            SlidingMenuHelper.getInstance().hideDemo();
            if (BluetoothConnection.getDefaultInstance() == null || BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_DISCONNECTED)
                reconnectToChip();
            else {
                showProgressDialog(getString(R.string.calibration_waiting));
                try {
                    if (Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareVersion()) <= 9
                            && Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareSubversion()) < 40) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (BluetoothConnection.getDefaultInstance() != null
                                        && BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_CONNECTED)
                                    hideProgressDialog();
                            }
                        }, 30000);
                        BluetoothConnection.getDefaultInstance().setCurrStatus(BluetoothConnection.STATUS_CALIBRATING);
                    }
                } catch (Exception ignored) {
                    hideProgressDialog();
                }
                ToolbarHelper.getInstance().setStatusConnected();
            }
        }
        return view;
    }

    private void reconnectToChip() {
        showProgressDialog("");
        ToolbarHelper.getInstance().setStatusConnecting();
        (new BluetoothConnection(BluetoothAdapter.getDefaultAdapter().getRemoteDevice(BluetoothConnection.getLastMac()), BluetoothConnection.getLastPassword()))
                .connect(new BluetoothConnectionInterface() {
                    @Override
                    public void connectionSuccess() {
                        RestHelper.getInstance().checkNeedUpgrade(BluetoothConnection.getDefaultInstance().getDeviceId(), new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String s) {
                                switch (s) {
                                    case "":
                                        RestHelper.getInstance().selectCarInterface.getReconfCount(BluetoothConnection.getDefaultInstance().getDeviceId()).enqueue(new Callback<ReconfCount>() {
                                            @Override
                                            public void onResponse(Call<ReconfCount> call, Response<ReconfCount> response) {
                                                BluetoothConnection.getDefaultInstance().setTunefilesLeft(response.body().getReconf_count());
                                            }

                                            @Override
                                            public void onFailure(Call<ReconfCount> call, Throwable t) {

                                            }
                                        });
                                        break;
                                    case "error":
                                        BluetoothConnection.setLastMac("");
                                        getActivity().getSupportFragmentManager().popBackStack();
                                        EventBus.getDefault().post(new TransactionFragment(new HomeFragment(), ""));
                                        SlidingMenuHelper.getInstance().enabled(false);
                                        break;
                                    default:
                                        BluetoothConnection.setLastMac("");
                                        getActivity().getSupportFragmentManager().popBackStack();
                                        EventBus.getDefault().post(new TransactionFragment(new HomeFragment(), ""));
                                        SlidingMenuHelper.getInstance().enabled(false);
                                        NeedUpgrade needUpgrade = new NeedUpgrade();
                                        Bundle bundle = new Bundle();
                                        bundle.putString(NeedUpgrade.URL, s);
                                        needUpgrade.setArguments(bundle);
                                        EventBus.getDefault().post(new TransactionFragment(needUpgrade, ""));
                                        break;
                                }
                            }
                        });

                        /*try {
                            if (Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareVersion()) <= 9
                                    && Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareSubversion()) < 40) {
                                if (getActivity() != null)
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (BluetoothConnection.getDefaultInstance() != null
                                                            && BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_CONNECTED)
                                                        hideProgressDialog();
                                                }
                                            }, 30000);
                                        }
                                    });
                                showProgressDialog(getString(R.string.calibration_waiting));
                            }
                        } catch (Exception ignored) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hideProgressDialog();
                                }
                            });
                        }*/
                        hideProgressDialog();
                    }

                    @Override
                    public void connectionFailed() {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    slideSwitch.setState(false);
                                    dashedCircularProgress.off();
                                    hideProgressDialog();
                                    ToolbarHelper.getInstance().setStatusDisconnected();
                                }
                            });
                    }

                    @Override
                    public void wrongPassword() {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    slideSwitch.setState(false);
                                    dashedCircularProgress.off();
                                    hideProgressDialog();
                                    ToolbarHelper.getInstance().setStatusDisconnected();
                                }
                            });
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        SlidingMenuHelper.getInstance().enabled(true);
        SlidingMenuHelper.getInstance().isClick(R.id.dashboard);
        ToolbarHelper.getInstance().showMainMenu("");
        ToolbarHelper.getInstance().showDashboardStatus();

        if (BluetoothConnection.getDefaultInstance() != null && !App.get().isDemo() && BluetoothConnection.getDefaultInstance().getCurrStatus() != BluetoothConnection.STATUS_DISCONNECTED) {
            switch (BluetoothConnection.getDefaultInstance().getDeviceStatus().getMode()) {
                case DeviceStatus.MODE_ECO:
                    slideSwitch.setState(true);
                    dashedCircularProgress.setEco();
                    nameMode.setText(getString(R.string.eco));
                    break;
                case DeviceStatus.MODE_SPORTPLUS:
                    slideSwitch.setState(true);
                    dashedCircularProgress.setSportPlus();
                    nameMode.setText(getString(R.string.sport_plus));
                    break;
                case DeviceStatus.MODE_SPORT:
                    slideSwitch.setState(true);
                    dashedCircularProgress.setSport();
                    nameMode.setText(getString(R.string.sport));
                    break;
                case DeviceStatus.MODE_STOCK:
                    slideSwitch.setState(false);
                    dashedCircularProgress.off();
                    nameMode.setText(getString(R.string.stock));
                    break;
            }
            if (BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_CONNECTED)
                ToolbarHelper.getInstance().setStatusConnected();
            else if (BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_CONNECTING)
                ToolbarHelper.getInstance().setStatusConnecting();
            else
                ToolbarHelper.getInstance().setStatusDisconnected();

            if (BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_CONNECTED || BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_DISCONNECTED)
                hideProgressDialog();
        } else {
            hideProgressDialog();
            slideSwitch.setState(false);
            dashedCircularProgress.off();
            nameMode.setText("");
            if (!App.get().isDemo()) {
                ToolbarHelper.getInstance().setStatusDisconnected();
            } else {
                ToolbarHelper.getInstance().setStatusConnected();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (App.get().isActivateDashboard() && !dashedCircularProgress.isAnimating()) {
            btSpp.setActivated(false);
            btSp.setActivated(false);
            btEco.setActivated(false);
            switch (v.getId()) {
                case R.id.button_sportp:
                    showProgressDialog("");
                    if (!App.get().isDemo()) {
                        BluetoothConnection.getDefaultInstance().commandSetMode(DeviceStatus.MODE_SPORTPLUS);
                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                slideSwitch.setState(true);
                                dashedCircularProgress.setSportPlus();
                                nameMode.setText(getString(R.string.sport_plus));
                                btSpp.setActivated(true);
                            }
                        }, 500);
                    }
                    break;
                case R.id.button_sport:
                    showProgressDialog("");
                    if (!App.get().isDemo()) {
                        BluetoothConnection.getDefaultInstance().commandSetMode(DeviceStatus.MODE_SPORT);
                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                slideSwitch.setState(true);
                                dashedCircularProgress.setSport();
                                nameMode.setText(getString(R.string.sport));
                                btSp.setActivated(true);
                            }
                        }, 1500);
                    }
                    break;
                case R.id.button_eco:
                    showProgressDialog("");
                    if (!App.get().isDemo()) {
                        BluetoothConnection.getDefaultInstance().commandSetMode(DeviceStatus.MODE_ECO);
                    } else {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                slideSwitch.setState(true);
                                dashedCircularProgress.setEco();
                                nameMode.setText(getString(R.string.eco));
                                btEco.setActivated(true);
                            }
                        }, 2500);
                    }
                    break;
            }
        }
        if (v.getId() == R.id.info) {
            new InfoDialogFragment().setType(true).show(getChildFragmentManager(), "info");
        }
    }

    @Override
    public void on() {
        if ((BluetoothConnection.getDefaultInstance() == null || BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_DISCONNECTED) && !App.get().isDemo()) {
            reconnectToChip();
            return;
        }
        App.get().setActivateDashboard(true);
        dashedCircularProgress.on();
        slide_off.setText(getString(R.string.slide_to_power_off));
        showSlideText();
        if (BluetoothConnection.getDefaultInstance() != null && BluetoothConnection.getDefaultInstance().getDeviceStatus().getMode().equals(DeviceStatus.MODE_STOCK) && !App.get().isDemo()) {
            showProgressDialog("");
            BluetoothConnection.getDefaultInstance().commandSetMode(DeviceStatus.MODE_SPORTPLUS);
            dashedCircularProgress.setSportPlus();
            nameMode.setText(getString(R.string.sport_plus));
        }
    }

    @Override
    public void off() {
        App.get().setActivateDashboard(false);
        slide_off.setText(getString(R.string.slide_to_power_on));
        showSlideText();
        if (BluetoothConnection.getDefaultInstance() != null
                && !BluetoothConnection.getDefaultInstance().getDeviceStatus().getMode().equals(DeviceStatus.MODE_STOCK)
                && BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_CONNECTED && !App.get().isDemo()) {
            showProgressDialog("");
            BluetoothConnection.getDefaultInstance().commandSetMode(DeviceStatus.MODE_STOCK);
        }
        if (App.get().isDemo()) {
            dashedCircularProgress.off();
            nameMode.setText(getString(R.string.stock));
        }
    }

    private void showSlideText() {
        slide_off.animate().alpha(1);
        slide_off.postDelayed(new Runnable() {
            @Override
            public void run() {
                slide_off.animate().alpha(0);
            }
        }, 1000);
    }

    @Override
    public void onValueChange(float value) {

    }

    @Override
    public void onAnimStarted() {
        slideSwitch.setSlideable(false);
    }

    @Override
    public void onAnimEnded() {
        slideSwitch.setSlideable(true);
    }

    @Override
    public void onEvent(EventBluetoothStatusReceived event) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ToolbarHelper.getInstance().setStatusConnected();
                try {
                    if (!(Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareVersion()) <= 9
                            && Integer.valueOf(BluetoothConnection.getDefaultInstance().getDeviceStatus().getSoftwareSubversion()) < 40))
                        hideProgressDialog();
                    else {
                        if (getActivity() != null)
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (BluetoothConnection.getDefaultInstance() != null
                                                    && BluetoothConnection.getDefaultInstance().getCurrStatus() == BluetoothConnection.STATUS_CONNECTED)
                                                hideProgressDialog();
                                        }
                                    }, 30000);
                                    showProgressDialog(getString(R.string.calibration_waiting));
                                }
                            });
                    }
                } catch (Exception ignored) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                        }
                    });
                }
                showStatusModeSet();
                btSpp.setActivated(false);
                btSp.setActivated(false);
                btEco.setActivated(false);
                switch (BluetoothConnection.getDefaultInstance().getDeviceStatus().getMode()) {
                    case DeviceStatus.MODE_ECO:
                        slideSwitch.setState(true);
                        dashedCircularProgress.setEco();
                        nameMode.setText(getString(R.string.eco));
                        btEco.setActivated(true);
                        break;
                    case DeviceStatus.MODE_SPORTPLUS:
                        slideSwitch.setState(true);
                        dashedCircularProgress.setSportPlus();
                        nameMode.setText(getString(R.string.sport_plus));
                        btSpp.setActivated(true);
                        break;
                    case DeviceStatus.MODE_SPORT:
                        slideSwitch.setState(true);
                        dashedCircularProgress.setSport();
                        nameMode.setText(getString(R.string.sport));
                        btSp.setActivated(true);
                        break;
                    case DeviceStatus.MODE_STOCK:
                        slideSwitch.setState(false);
                        dashedCircularProgress.off();
                        nameMode.setText(getString(R.string.stock));
                        break;
                }
            }
        });
    }

    @Override
    public void onEvent(EventBluetoothError event) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressDialog();
                nameMode.setText("");
                ToolbarHelper.getInstance().setStatusDisconnected();
                slideSwitch.setState(false);
                dashedCircularProgress.off();
                showStatusConnectingProblem();
            }
        });
    }
}
