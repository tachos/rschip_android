package it.rschip.rschip;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;

import com.google.gson.Gson;

import it.rschip.rschip.models.ModificationItem;
import it.rschip.rschip.utils.LocaleHelper;

/**
 * Created by Dan on 23.05.2016.
 */
public class App extends Application {

    private static final String LANGUAGE = "language";
    private static final String MODE = "activation_mode";
    private static final String SELECTION_MODE = "selection_mode";
    private static final String SELECTION_STAGE = "stage";
    private static final String MARK = "mark";
    private static final String MARK_ID = "mark_id";
    private static final String MODEL = "model";
    private static final String MODEL_ID = "model_id";
    private static final String MODIFICATION = "modification";
    private static final String ACTIVATE_DASHBOARD = "activate_dasboard";
    private static App instance;
    private SharedPreferences preferences;

    private boolean demo = false;

    public static App get() {
        return instance;
    }

    public SharedPreferences getPreferences() {
        return preferences;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        LocaleHelper.get().onCreate(this);
    }

    public void setLanguage(int position) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(LANGUAGE, position);
        editor.commit();
    }

    public int getPositionLanguage() {
        return preferences.getInt(LANGUAGE, 0);
    }

    public void setActivationMode(int position) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(MODE, position);
        editor.commit();
    }

    public int getPositionActivationMode() {
        return preferences.getInt(MODE, 0);
    }

    public boolean isDemo() {
        return demo;
    }

    public void setDemo(boolean b) {
        demo = b;
    }

    public boolean isSelectionMode() {
        return preferences.getBoolean(SELECTION_MODE, false);
    }

    public void setSelectionMode(boolean b) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SELECTION_MODE, b);
        editor.commit();
    }

    public int getSelectionStage() {
        return preferences.getInt(SELECTION_STAGE, 0);
    }

    public void setSelectionStage(int stage) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(SELECTION_STAGE, stage);
        editor.commit();
    }

    public void setMark(String mark, int id) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MARK, mark);
        editor.putInt(MARK_ID, id);
        editor.commit();
    }

    public String getMark() {
        return preferences.getString(MARK, "");
    }

    public int getMarkId() {
        return preferences.getInt(MARK_ID, -1);
    }

    public void setModel(String model, int id) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MODEL, model);
        editor.putInt(MODEL_ID, id);
        editor.commit();
    }

    public String getModel() {
        return preferences.getString(MODEL, "");
    }

    public int getModelId() {
        return preferences.getInt(MODEL_ID, -1);
    }

    public ModificationItem getModification() {
        return new Gson().fromJson(preferences.getString(MODIFICATION, "{\"id\":9996773,\"title\":\"(I) 2.0i\",\"hp\":115,\"nm\":170}"), ModificationItem.class);
    }

    public void setModification(ModificationItem modification) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MODIFICATION, new Gson().toJson(modification));
        editor.commit();
    }

    public boolean isActivateDashboard() {
        return preferences.getBoolean(ACTIVATE_DASHBOARD, true);
    }

    public void setActivateDashboard(boolean b) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(ACTIVATE_DASHBOARD, b);
        editor.commit();
    }
}
