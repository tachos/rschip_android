package it.rschip.rschip.utils;

import android.support.v4.app.Fragment;

/**
 * Created by Dan on 19.05.2016.
 */
public class TransactionFragment {

    private Fragment fragment;
    private String title;

    public TransactionFragment(Fragment fragment, String title) {
        this.fragment = fragment;
        this.title = title;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public String getTitle() {
        return title;
    }
}
