package it.rschip.rschip.utils;

import android.webkit.ValueCallback;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import it.rschip.rschip.App;
import it.rschip.rschip.interfaces.APIDealers;
import it.rschip.rschip.interfaces.APIRest;
import it.rschip.rschip.models.CheckUpgrade;
import it.rschip.rschip.models.Configuration;
import it.rschip.rschip.models.Dealer;
import it.rschip.rschip.models.Settings;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dan on 23.05.2016.
 */
public class RestHelper {
    private static RestHelper instance;
    public APIRest selectCarInterface;
    public Response<List<Dealer>> responseDealers;
    private OkHttpClient okHttpClient;
    private APIDealers dealersInterface;

    public RestHelper() {
        okHttpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://tuning34.ru/api/v1/")
                .client(okHttpClient)
                .build();
        dealersInterface = retrofit.create(APIDealers.class);
        selectCarInterface = retrofit.create(APIRest.class);
    }

    public static RestHelper getInstance() {
        if (instance == null) {
            instance = new RestHelper();
        }
        return instance;
    }

    public synchronized void getDealers(final Callback<List<Dealer>> callback) {
        if (responseDealers != null) {
            callback.onResponse(null, responseDealers);
        } else {
            dealersInterface.getDealers().enqueue(new Callback<List<Dealer>>() {
                @Override
                public void onResponse(Call<List<Dealer>> call, Response<List<Dealer>> response) {
                    responseDealers = response;
                    callback.onResponse(call, response);
                }

                @Override
                public void onFailure(Call<List<Dealer>> call, Throwable t) {
                    callback.onFailure(call, t);
                }
            });
        }
    }

    public void getSettings(int modification, final CallbackFile callbackFile) {
        selectCarInterface.getSettings(modification).enqueue(new Callback<Settings>() {
            @Override
            public void onResponse(final Call<Settings> call, final Response<Settings> responseSettings) {
                String file_url;
                if (responseSettings.body().getData() != null && responseSettings.body().getData().size() > 0) {
                    file_url = "http://tuning34.ru" + responseSettings.body().getData().get(0).getFile_url();

                    okHttpClient.newCall(new Request.Builder().url(file_url).build()).enqueue(new okhttp3.Callback() {
                        @Override
                        public void onFailure(okhttp3.Call call, IOException e) {
                            if (callbackFile != null) callbackFile.result("", null);
                        }

                        @Override
                        public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                            if (callbackFile != null)
                                callbackFile.result(response.body().string(), responseSettings.body().getData().get(0));
                        }
                    });
                } else if (callbackFile != null) callbackFile.result("", null);
            }

            @Override
            public void onFailure(Call<Settings> call, Throwable t) {
                if (callbackFile != null) callbackFile.result("", null);
            }
        });
    }

    public void RegistrationConfiguration(String serial_number, String modification_id, final CallbackStatus callback) {
        selectCarInterface.putRegistrationConfiguration(new Configuration(serial_number, android.provider.Settings.Secure.getString(App.get().getApplicationContext().getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID), modification_id, "Android")).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (callback != null) {
                    if (response.code() == 200 || response.code() == 201) callback.result(true);
                    else callback.result(false);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (callback != null) callback.result(false);
                t.printStackTrace();
            }
        });
    }

    public void sendLogField(final CallbackStatus callback) {
        if (LoggingHelper.getInstance().exists()) {
            selectCarInterface.sendLogFile(MultipartBody.Part.createFormData("file", LoggingHelper.getInstance().getLogFile().getName(), RequestBody.create(MediaType.parse("multipart/form-data"),
                    LoggingHelper.getInstance().getLogFile())), Utils.getDeviceName(), android.provider.Settings.Secure.getString(App.get().getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ANDROID_ID), Utils.getAndroidVersion()).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    if (callback != null) {
                        if (response.code() == 201) callback.result(true);
                        else callback.result(false);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    if (callback != null) callback.result(false);
                }
            });
        } else {
            if (callback != null) callback.result(false);
        }
    }

    public void checkNeedUpgrade(String serial_number, final ValueCallback<String> callback) {
        selectCarInterface.getCheckNeedUpgrade(serial_number).enqueue(new Callback<CheckUpgrade>() {
            @Override
            public void onResponse(Call<CheckUpgrade> call, Response<CheckUpgrade> response) {
                switch (response.code()) {
                    case 200:
                        callback.onReceiveValue("");
                        break;
                    case 426:
                        if (response.errorBody() != null) {
                            try {
                                CheckUpgrade checkUpgrade = new Gson().fromJson(new String(response.errorBody().bytes()), CheckUpgrade.class);
                                if (checkUpgrade.getError().equals("17")) {
                                    callback.onReceiveValue(checkUpgrade.getData().getLink_to_android());
                                } else {
                                    callback.onReceiveValue("error");
                                }
                            } catch (IOException e) {
                                callback.onReceiveValue("error");
                                e.printStackTrace();
                            }
                        }
                        break;
                    default:
                        callback.onReceiveValue("error");
                        break;
                }
            }

            @Override
            public void onFailure(Call<CheckUpgrade> call, Throwable t) {
                callback.onReceiveValue("error");
            }
        });
    }

    public interface CallbackFile {
        void result(String content, Settings.Item settings);
    }

    public interface CallbackStatus {
        void result(boolean status);
    }
}
