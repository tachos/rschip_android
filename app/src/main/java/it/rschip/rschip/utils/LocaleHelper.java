package it.rschip.rschip.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import java.util.Locale;

import it.rschip.rschip.App;

/**
 * Created by Dan on 06.06.2016.
 */
public class LocaleHelper {

    private static final String SELECTED_LANGUAGE = "Locale.Helper.Selected.Language";
    private static final String SELECTED_COUNTRY = "Locale.Helper.Selected.Country";

    private static LocaleHelper instance;
    private SharedPreferences preferences;

    public static LocaleHelper get() {
        if (instance == null) {
            instance = new LocaleHelper();
        }
        return instance;
    }

    public void onCreate(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String lang = getPersistedDataLanguage(Locale.getDefault().getLanguage());
        String country = getPersistedDataCountry("");
        selectLanguage(lang);
        setLocale(context, lang, country);
    }

    private void selectLanguage(String lang) {
        int position = 0;
        switch (lang) {
            case "en":
                position = 0;
                break;
            case "es":
                position = 1;
                break;
            case "zh":
                position = 2;
                break;
            case "ru":
                position = 3;
                break;
        }
        App.get().setLanguage(position);
    }

    public void setLocale(Context context, String language, String country) {
        persist(language, country);

        Locale locale = new Locale(language, country);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    private String getPersistedDataLanguage(String defaultLanguage) {
        return preferences.getString(SELECTED_LANGUAGE, defaultLanguage);
    }

    private String getPersistedDataCountry(String defaultCountry) {
        return preferences.getString(SELECTED_COUNTRY, defaultCountry);
    }

    private void persist(String language, String country) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SELECTED_LANGUAGE, language);
        editor.putString(SELECTED_COUNTRY, country);
        editor.apply();
    }
}