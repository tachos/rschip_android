package it.rschip.rschip.utils;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import it.rschip.rschip.R;

/**
 * Created by Dan on 19.05.2016.
 */
public class ToolbarHelper {

    private static ToolbarHelper instance;

    private Context mContext;
    private ImageView mMenu;
    private TextView mTitle;
    private TextView mStatus;
    private TextView mChipStatus;
    private ImageView mStatusIcon;

    private View.OnClickListener mMenuOnClickListener;
    private View.OnClickListener mBackClickListener;

    public ToolbarHelper(Context context, View parent) {
        instance = this;
        this.mContext = context;
        this.mMenu = (ImageView) parent.findViewById(R.id.menu_button);
        this.mTitle = (TextView) parent.findViewById(R.id.title);
        this.mStatus = (TextView) parent.findViewById(R.id.status);
        this.mChipStatus = (TextView) parent.findViewById(R.id.chipStatus);
        this.mStatusIcon = (ImageView) parent.findViewById(R.id.statusIcon);
    }

    public static ToolbarHelper getInstance() {
        return instance;
    }

    public void setTitle(String title) {
        this.mTitle.setText(title);
    }

    public void showMainMenu(String title) {
        this.mTitle.setText(title);
        this.mMenu.setImageResource(R.drawable.ic_menu_button);
        this.mTitle.setVisibility(View.VISIBLE);
        this.mStatus.setVisibility(View.GONE);
        this.mStatusIcon.setVisibility(View.GONE);
        this.mMenu.setOnClickListener(mMenuOnClickListener);
    }

    public void showBack(String title) {
        this.mTitle.setText(title);
        this.mMenu.setImageResource(R.drawable.ic_arrow_left);
        this.mTitle.setVisibility(View.VISIBLE);
        this.mStatus.setVisibility(View.GONE);
        this.mStatusIcon.setVisibility(View.GONE);
        this.mMenu.setOnClickListener(mBackClickListener);
    }

    public void showDashboardStatus() {
        this.mTitle.setVisibility(View.GONE);
        this.mStatus.setVisibility(View.VISIBLE);
        this.mStatusIcon.setVisibility(View.VISIBLE);
    }

    public void setStatusConnecting() {
        this.mStatus.setText(mContext.getResources().getString(R.string.Connecting));
        this.mStatusIcon.setImageResource(R.drawable.icon_connecting);
    }

    public void setStatusConnected() {
        this.mStatus.setText(mContext.getResources().getString(R.string.Connected));
        this.mStatusIcon.setImageResource(R.drawable.icon_connected);
    }

    public void setStatusDisconnected() {
        this.mStatus.setText(mContext.getResources().getString(R.string.Disconnected));
        this.mStatusIcon.setImageResource(R.drawable.icon_disconnected);
    }

    public TextView getChipStatus() {
        return mChipStatus;
    }

    public void setClickListenerMenu(final View.OnClickListener listener) {
        mMenuOnClickListener = listener;
    }

    public void setClickListenerBack(final View.OnClickListener listener) {
        mBackClickListener = listener;
    }
}
