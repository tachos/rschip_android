package it.rschip.rschip.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.rschip.rschip.App;

/**
 * Created by Dan on 01.06.2016.
 */
public class LoggingHelper {

    private static LoggingHelper instance;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private File logFile;

    public LoggingHelper() {
        logFile = new File(App.get().getFilesDir().getAbsolutePath() + "/RSCHIP.log");
    }

    public static LoggingHelper getInstance() {
        if (instance == null) {
            instance = new LoggingHelper();
        }
        return instance;
    }

    public void appendLog(String message) {
        String text_log = dateFormat.format(new Date(System.currentTimeMillis())) + " - " + message;
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text_log);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean exists() {
        return logFile.exists();
    }

    public File getLogFile() {
        if (logFile.exists()) {
            return logFile;
        }
        return null;
    }

    public String getAllLog() {
        StringBuilder log = new StringBuilder();
        if (logFile.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(logFile));
                String line;
                while ((line = br.readLine()) != null) {
                    log.append(line);
                }
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return log.toString();
    }

    public void deleteLogFile() {
        if (exists()) logFile.delete();
    }

    public boolean clear() {
        return logFile.delete();
    }
}
