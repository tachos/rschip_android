package it.rschip.rschip.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import it.rschip.rschip.App;
import it.rschip.rschip.R;
import it.rschip.rschip.models.ModificationItem;

/**
 * Created by Dan on 25.05.2016.
 */
public class SlidingMenuHelper {

    private static SlidingMenuHelper instance;
    private Context mContext;
    private SlidingMenu mMenu;
    private TextView mMarkModel;
    private TextView mModification;
    private TextView mHp;
    private TextView mNm;
    private TextView home;
    private TextView dashboard;
    private TextView settings;
    private TextView contact_us;
    private TextView exit_demo;
    private int lastIdButton = -1;

    public SlidingMenuHelper(Activity activity, View parent, View.OnClickListener onClickListener) {
        instance = this;
        mContext = activity;
        mMenu = new SlidingMenu(activity);
        mMenu.setMode(SlidingMenu.LEFT);
        mMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        mMenu.setShadowWidthRes(R.dimen._5sdp);
        mMenu.setShadowDrawable(ContextCompat.getDrawable(mContext, R.drawable.shadow));
        mMenu.setBehindOffsetRes(R.dimen._53sdp);
        mMenu.setFadeDegree(0.35f);
        mMenu.attachToActivity(activity, SlidingMenu.SLIDING_CONTENT);
        mMenu.setMenu(R.layout.drawer_menu);
        mMenu.setSlidingEnabled(false);

        mMarkModel = (TextView) parent.findViewById(R.id.mark_model);
        mModification = (TextView) parent.findViewById(R.id.modification);
        mHp = (TextView) parent.findViewById(R.id.hp);
        mNm = (TextView) parent.findViewById(R.id.nm);
        home = (TextView) parent.findViewById(R.id.home);
        dashboard = (TextView) parent.findViewById(R.id.dashboard);
        settings = (TextView) parent.findViewById(R.id.settings);
        contact_us = (TextView) parent.findViewById(R.id.contact_us);
        exit_demo = (TextView) parent.findViewById(R.id.exit_demo);

        home.setOnClickListener(onClickListener);
        dashboard.setOnClickListener(onClickListener);
        settings.setOnClickListener(onClickListener);
        contact_us.setOnClickListener(onClickListener);
        exit_demo.setOnClickListener(onClickListener);
    }

    public static SlidingMenuHelper getInstance() {
        return instance;
    }

    public boolean isClick(int id) {
        if (lastIdButton != id) {
            lastIdButton = id;
            return true;
        } else
            return false;
    }

    public boolean isShowing() {
        return mMenu.isMenuShowing();
    }

    public void enabled(boolean enabled) {
        mMenu.setSlidingEnabled(enabled);
    }

    public void show() {
        if (!mMenu.isMenuShowing()) mMenu.showMenu(true);
    }

    public void hide() {
        if (mMenu.isMenuShowing()) mMenu.showContent(true);
    }

    public void setDemo() {
        exit_demo.setVisibility(View.VISIBLE);
        mMarkModel.setText(mContext.getString(R.string.demo_mark_model));
        mModification.setText(mContext.getString(R.string.demo_modification));
        mHp.setText(mContext.getString(R.string.demo_ph));
        mNm.setText(mContext.getString(R.string.demo_nm));
    }

    public void hideDemo() {
        exit_demo.setVisibility(View.GONE);
    }

    public void setActivate() {
        mMarkModel.setText(App.get().getMark() + " " + App.get().getModel());
        ModificationItem item = App.get().getModification();
        mModification.setText(item.getTitle());
        mHp.setText(item.getHp() + " hp");
        mNm.setText(item.getNm() + " nm");
    }

    public void updateViews() {
        Resources resources = mContext.getResources();
        home.setText(resources.getString(R.string.home));
        dashboard.setText(resources.getString(R.string.driving_mode));
        settings.setText(resources.getString(R.string.settings));
        contact_us.setText(resources.getString(R.string.contact_us));
        exit_demo.setText(resources.getString(R.string.ExitDemoversion));
    }
}
