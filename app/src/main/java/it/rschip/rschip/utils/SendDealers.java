package it.rschip.rschip.utils;

import java.util.List;

import it.rschip.rschip.models.Dealer;

/**
 * Created by Dan on 24.05.2016.
 */
public class SendDealers {

    private List<Dealer> dealers;

    public SendDealers(List<Dealer> dealers) {
        this.dealers = dealers;
    }

    public List<Dealer> getDealers() {
        return dealers;
    }
}
