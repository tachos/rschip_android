package it.rschip.rschip.utils;

/**
 * Created by Dan on 23.05.2016.
 */
public class TransactionHomeFragment {

    private int position;

    public TransactionHomeFragment(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
