package it.rschip.rschip.interfaces;

import java.util.List;

import it.rschip.rschip.models.Dealer;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Dan on 23.05.2016.
 */
public interface APIDealers {
    @GET("media/dealers_list.json")
    Call<List<Dealer>> getDealers();
}
