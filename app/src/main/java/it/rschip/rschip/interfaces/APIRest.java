package it.rschip.rschip.interfaces;

import com.google.gson.JsonObject;

import it.rschip.rschip.models.Car;
import it.rschip.rschip.models.CheckUpgrade;
import it.rschip.rschip.models.Configuration;
import it.rschip.rschip.models.Modification;
import it.rschip.rschip.models.ReconfCount;
import it.rschip.rschip.models.Settings;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Dan on 26.05.2016.
 */
public interface APIRest {
    @GET("chip/reconf-count/{serial_number}")
    Call<ReconfCount> getReconfCount(@Path("serial_number") String serial_number);

    @GET("catalog/brands/?format=json&type=atmo")
    Call<Car> getMarks();

    @GET("catalog/models/?format=json&type=atmo")
    Call<Car> getModels(@Query("brand") int brand);

    @GET("catalog/modifications/?format=json&type=atmo")
    Call<Modification> getModifications(@Query("model") int model);

    @GET("catalog/modifications/{modification}")
    Call<JsonObject> getModification(@Path("modification") String modification);

    @GET("catalog/settings/?format=json&type=atmo")
    Call<Settings> getSettings(@Query("modification") int modification);

    @PUT("chip/registrations-configuration/")
    Call<Void> putRegistrationConfiguration(@Body Configuration configuration);

    @GET("chip/check-need-upgrade/{serial_number}")
    Call<CheckUpgrade> getCheckNeedUpgrade(@Path("serial_number") String serial_number);

    @Multipart
    @POST("device/log/")
    Call<JsonObject> sendLogFile(@Part MultipartBody.Part file, @Part("device_model") String device_model, @Part("device_id") String device_id, @Part("device_type") String device_type);
}