package it.rschip.rschip.bluetooth.commands;

public class BluetoothCommandRestore extends BluetoothCommand {
    @Override
    public String getCommand() {
        return "\nRestore\0";
    }

    @Override
    public int getCode() {
        return COMMAND_RESTORE;
    }
}
