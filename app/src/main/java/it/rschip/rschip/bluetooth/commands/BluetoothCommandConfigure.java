package it.rschip.rschip.bluetooth.commands;

public class BluetoothCommandConfigure extends BluetoothCommand {
    private String command;

    public BluetoothCommandConfigure(String modification, String programVersion, String param1, String param2, String param3, String program) {
        command = "\nConfigS " +
                modification + " " +
                programVersion + " " + param1 + param2 + param3 + " " +
                program + "\0";
    }

    @Override
    public String getCommand() {
        return command;
    }

    @Override
    public int getCode() {
        return COMMAND_CONFIGURE;
    }
}
