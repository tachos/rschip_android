package it.rschip.rschip.bluetooth.commands;

public class BluetoothCommandGetStatus extends BluetoothCommand {
    @Override
    public String getCommand() {
        return "\nGet_Status\0";
    }

    @Override
    public int getCode() {
        return COMMAND_GET_STATUS;
    }
}
