package it.rschip.rschip.bluetooth.commands;

public class BluetoothCommandSetMode extends BluetoothCommand {
    private String command;

    public BluetoothCommandSetMode(String mode) {
        command = "\nSetMode " + mode + " +00+00+00\0";
    }

    @Override
    public String getCommand() {
        return command;
    }

    @Override
    public int getCode() {
        return COMMAND_SET_MODE;
    }
}
