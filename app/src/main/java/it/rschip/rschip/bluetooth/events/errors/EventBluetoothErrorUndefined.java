package it.rschip.rschip.bluetooth.events.errors;

public class EventBluetoothErrorUndefined extends EventBluetoothError {

    @Override
    public String getDescription() {
        return "Unknown error";
    }

    @Override
    public int getCode() {
        return ERROR_UNDEFINED;
    }
}