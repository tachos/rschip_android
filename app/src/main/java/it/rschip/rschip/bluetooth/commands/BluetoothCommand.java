package it.rschip.rschip.bluetooth.commands;

public abstract class BluetoothCommand {
    public static final int COMMAND_PING = 0;
    public static final int COMMAND_AUTH = 1;
    public static final int COMMAND_RESET = 2;
    public static final int COMMAND_PREPARE_CONFIG = 3;
    public static final int COMMAND_CONFIGURE = 4;
    public static final int COMMAND_GET_STATUS = 5;
    public static final int COMMAND_GET_DEVICE_ID = 6;
    public static final int COMMAND_SET_MODE = 7;
    public static final int COMMAND_RESTORE = 8;
    public static final int COMMAND_SETFWMODE = 9;
    public static final int COMMAND_GETFWMODE = 10;

    abstract public String getCommand();

    abstract public int getCode();
}
