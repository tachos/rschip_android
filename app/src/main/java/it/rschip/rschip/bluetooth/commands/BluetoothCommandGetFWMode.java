package it.rschip.rschip.bluetooth.commands;

public class BluetoothCommandGetFWMode extends BluetoothCommand {
    @Override
    public String getCommand() {
        return "\nFW Get\0";
    }

    @Override
    public int getCode() {
        return COMMAND_GETFWMODE;
    }
}
