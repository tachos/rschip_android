package it.rschip.rschip.bluetooth.commands;

public class BluetoothCommandGetDevId extends BluetoothCommand {
    @Override
    public String getCommand() {
        return "\nGet_id\0";
    }

    @Override
    public int getCode() {
        return COMMAND_GET_DEVICE_ID;
    }
}
