package it.rschip.rschip.bluetooth.commands;

public class BluetoothCommandSetFWMode extends BluetoothCommand {
    public static final String MODE_A = "A";
    public static final String MODE_B = "B";
    public static final String MODE_X = "X";

    private String modeToSet;

    public BluetoothCommandSetFWMode(String mode) {
        modeToSet = mode;
    }

    @Override
    public String getCommand() {
        return "\nFW Set " + modeToSet + "\0";
    }

    @Override
    public int getCode() {
        return COMMAND_SETFWMODE;
    }
}
