package it.rschip.rschip.bluetooth.events.errors;

public class EventBluetoothErrorTimeout extends EventBluetoothError {

    @Override
    public String getDescription() {
        return "Connection timeout";
    }

    @Override
    public int getCode() {
        return ERROR_TIMEOUT;
    }
}
