package it.rschip.rschip.bluetooth.commands;

public class BluetoothCommandPing extends BluetoothCommand {
    @Override
    public String getCommand() {
        return "\nPing\0";
    }

    @Override
    public int getCode() {
        return COMMAND_PING;
    }
}
