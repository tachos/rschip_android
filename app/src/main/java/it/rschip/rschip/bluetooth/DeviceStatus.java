package it.rschip.rschip.bluetooth;

public class DeviceStatus {
    public static final String MODE_SPORT = "spt";
    public static final String MODE_SPORTPLUS = "spp";
    public static final String MODE_STOCK = "stc";
    public static final String MODE_ECO = "eco";

    private String softwareVersion;
    private String softwareSubversion;

    private String modification = "";
    private String mode = MODE_SPORTPLUS;
    private int reconfCount = 1;
    private String programVersion = "";
    private String program1;
    private String program2;
    private String program3;
    private String FWMode = "";

    public String getProgramVersion() {
        return programVersion;
    }

    public String getSoftwareVersion() {
        return softwareVersion;
    }

    public String getSoftwareSubversion() {
        return softwareSubversion;
    }

    public String getModification() {
        return modification;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mod) {
        mode = mod;
    }

    public String getProgram1() {
        return program1;
    }

    public String getProgram2() {
        return program2;
    }

    public String getProgram3() {
        return program3;
    }

    public boolean parseStatus(String status) {
        if (status == null) return false;
        if (status.isEmpty()) return false;

        try {
            softwareVersion = status.substring(0, 2);
            status = status.substring(3);
            softwareSubversion = status.substring(0, 2);
            status = status.substring(3);
            modification = status.substring(0, status.indexOf(" "));
            while (modification.length() < 15) {
                modification = "0" + modification;
            }

            status = status.substring(status.indexOf(" ") + 1);
            programVersion = status.substring(0, status.indexOf(" "));
            status = status.substring(status.indexOf(" ") + 1);

            program1 = status.substring(0, 3);
            status = status.substring(3);
            program2 = status.substring(0, 3);
            status = status.substring(3);
            program3 = status.substring(0, 3);
            status = status.substring(4);

            mode = status.substring(0, status.indexOf(" "));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getReconfCount() {
        return reconfCount;
    }

    public void setReconfCount(int reconfCount) {
        this.reconfCount = reconfCount;
    }


    public String getFWMode() {
        return FWMode;
    }

    public void setFWMode(String FWMode) {
        this.FWMode = FWMode;
    }
}
