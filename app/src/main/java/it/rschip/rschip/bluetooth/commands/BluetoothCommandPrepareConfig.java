package it.rschip.rschip.bluetooth.commands;

public class BluetoothCommandPrepareConfig extends BluetoothCommand {
    @Override
    public String getCommand() {
        return "\nPrepareConfig\0";
    }

    @Override
    public int getCode() {
        return COMMAND_PREPARE_CONFIG;
    }
}
