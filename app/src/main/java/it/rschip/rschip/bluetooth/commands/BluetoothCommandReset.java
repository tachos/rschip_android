package it.rschip.rschip.bluetooth.commands;

public class BluetoothCommandReset extends BluetoothCommand {
    @Override
    public String getCommand() {
        return "\nReset\0";
    }

    @Override
    public int getCode() {
        return COMMAND_RESET;
    }
}
