package it.rschip.rschip.bluetooth;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import it.rschip.rschip.App;
import it.rschip.rschip.bluetooth.commands.BluetoothCommand;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandAuth;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandConfigure;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandGetDevId;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandGetFWMode;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandGetStatus;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandPing;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandPrepareConfig;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandReset;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandRestore;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandSetFWMode;
import it.rschip.rschip.bluetooth.commands.BluetoothCommandSetMode;
import it.rschip.rschip.bluetooth.events.EventBluetoothStatusReceived;
import it.rschip.rschip.bluetooth.events.calibrating.EventBluetoothCalibratingCompleted;
import it.rschip.rschip.bluetooth.events.calibrating.EventBluetoothCalibratingInterrupted;
import it.rschip.rschip.bluetooth.events.calibrating.EventBluetoothCalibratingProgress;
import it.rschip.rschip.bluetooth.events.errors.EventBluetoothErrorConnectionLost;
import it.rschip.rschip.bluetooth.events.errors.EventBluetoothErrorTimeout;
import it.rschip.rschip.models.Settings;
import it.rschip.rschip.utils.LoggingHelper;

public class BluetoothConnection {
    public static final int STATUS_DISCONNECTED = 0;
    public static final int STATUS_CONNECTED = 1;
    public static final int STATUS_CONNECTING = 2;
    public static final int STATUS_BUSY = 3;
    public static final int STATUS_CALIBRATING = 4;
    public static final int STATUS_ERROR = 5;
    private static final String BLUETOOTH_LAST_CONNECTED_MAC = "last_bluetooth_device_mac";
    private static final String BLUETOOTH_LAST_CONNECTED_NAME = "last_bluetooth_device_name";
    private static final String BLUETOOTH_LAST_CONNECTED_PASSWORD = "last_bluetooth_device_password";
    private static final String BLUETOOTH_LAST_CONNECTED_SETTING_ITEM = "last_bluetooth_device_settings_item";
    private static final String BLUETOOTH_LAST_CONNECTED_SETTING_CONTENT = "last_bluetooth_device_settings_content";
    private static BluetoothConnection defaultInstance;
    private ArrayList<BluetoothCommand> commandQueue = new ArrayList<>();
    private int currStatus = STATUS_DISCONNECTED;
    private BluetoothDevice device;
    private String name;
    private String password;
    private ConnectionThread connectionThread;
    private DeviceStatus deviceStatus = new DeviceStatus();
    private String deviceId = "";
    private int tunefilesLeft = 0;

    @SuppressLint("MissingPermission")
    public BluetoothConnection(BluetoothDevice device, String password) {
        defaultInstance = this;
        if (device.getName() != null) if (!device.getName().isEmpty()) name = device.getName();
        else name = "RSchip";

        this.device = device;
        this.password = password;
    }

    public static void reset() {
        setLastMac("");
        setLastName("");
        setLastPassword("");
        setLastSettingsItemContent("");
        setLastSettingsItem(
                new Gson().fromJson(App.get().getPreferences().getString(BLUETOOTH_LAST_CONNECTED_SETTING_ITEM,
                        "{\"param1\":\"+00\",\"param2\":\"+00\",\"param3\":\"+00\",\"title\":\"0001056\",\"file_url\":\"/media/files/1056.txt\"}"), Settings.Item.class));
    }

    public static BluetoothConnection getDefaultInstance() {
        return defaultInstance;
    }

    public static String getLastMac() {
        return App.get().getPreferences().getString(BLUETOOTH_LAST_CONNECTED_MAC, "");
    }

    public static void setLastMac(String mac) {
        SharedPreferences.Editor editor = App.get().getPreferences().edit();
        editor.putString(BLUETOOTH_LAST_CONNECTED_MAC, mac);
        editor.commit();
    }

    public static String getLastName() {
        return App.get().getPreferences().getString(BLUETOOTH_LAST_CONNECTED_NAME, "");
    }

    public static void setLastName(String name) {
        SharedPreferences.Editor editor = App.get().getPreferences().edit();
        editor.putString(BLUETOOTH_LAST_CONNECTED_NAME, name);
        editor.commit();
    }

    public static String getLastPassword() {
        return App.get().getPreferences().getString(BLUETOOTH_LAST_CONNECTED_PASSWORD, "");
    }

    public static void setLastPassword(String pass) {
        SharedPreferences.Editor editor = App.get().getPreferences().edit();
        editor.putString(BLUETOOTH_LAST_CONNECTED_PASSWORD, pass);
        editor.commit();
    }

    public static Settings.Item getLastSettingsItem() {
        return new Gson().fromJson(App.get().getPreferences().getString(BLUETOOTH_LAST_CONNECTED_SETTING_ITEM, "{\"param1\":\"+00\",\"param2\":\"+00\",\"param3\":\"+00\",\"title\":\"0001056\",\"file_url\":\"/media/files/1056.txt\"}"), Settings.Item.class);
    }

    public static void setLastSettingsItem(Settings.Item item) {
        SharedPreferences.Editor editor = App.get().getPreferences().edit();
        editor.putString(BLUETOOTH_LAST_CONNECTED_SETTING_ITEM, new Gson().toJson(item));
        editor.commit();
    }

    public static String getLastSettingsItemContent() {
        return App.get().getPreferences().getString(BLUETOOTH_LAST_CONNECTED_SETTING_CONTENT, "");
    }

    public static void setLastSettingsItemContent(String content) {
        SharedPreferences.Editor editor = App.get().getPreferences().edit();
        editor.putString(BLUETOOTH_LAST_CONNECTED_SETTING_CONTENT, content);
        editor.commit();
    }

    public String getMac() {
        return device.getAddress();
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public int getTunefilesLeft() {
        return tunefilesLeft;
    }

    public void setTunefilesLeft(int tunefilesLeft) {
        this.tunefilesLeft = tunefilesLeft;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public DeviceStatus getDeviceStatus() {
        return deviceStatus;
    }

    @SuppressLint("MissingPermission")
    public void connect(BluetoothConnectionInterface callback) {
        currStatus = STATUS_CONNECTING;
        try {
            if (connectionThread != null) connectionThread.interrupt();
            connectionThread = new ConnectionThread(device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb")), callback);
            connectionThread.start();
        } catch (IOException connectException) {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (!mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.enable();
                connect(callback);
            } else {
                currStatus = STATUS_DISCONNECTED;
                connectException.printStackTrace();
                callback.connectionFailed();
            }
        }
    }

    public int getCurrStatus() {
        return currStatus;
    }

    public void setCurrStatus(int currStatus) {
        this.currStatus = currStatus;
    }

    private void addCommandToQueue(BluetoothCommand command) {
        if (connectionThread == null)
            EventBus.getDefault().post(new EventBluetoothErrorConnectionLost());
        commandQueue.add(command);
        if (commandQueue.size() == 1) {
            if (currStatus == STATUS_CONNECTED) {
                commandQueue.add(0, new BluetoothCommandAuth(password));
                commandQueue.add(0, new BluetoothCommandPing());
                connectionThread.write(commandQueue.get(0));
            } else
                connectionThread.write(command);
        }
    }

    private void commandPing() {
        addCommandToQueue(new BluetoothCommandPing());
    }

    private void commandAuth() {
        addCommandToQueue(new BluetoothCommandAuth(password));
    }

    private void commandPrepareConfig() {
        addCommandToQueue(new BluetoothCommandPrepareConfig());
    }

    public void commandConfigure(String modId, String progrVersion, String param1, String param2, String param3, String program) {
        commandPrepareConfig();
        addCommandToQueue(new BluetoothCommandConfigure(modId, progrVersion, param1, param2, param3, program));
        commandSetMode("spp");
        commandGetStatus();
    }

    public void commandGetStatus() {
        addCommandToQueue(new BluetoothCommandGetFWMode());
        addCommandToQueue(new BluetoothCommandGetStatus());
    }

    public void commandSetMode(String mode) {
        addCommandToQueue(new BluetoothCommandSetMode(mode));
        commandGetStatus();
    }

    public void commandSetFWMode(String mode) {
        addCommandToQueue(new BluetoothCommandSetFWMode(mode));
        commandGetStatus();
    }

    public void commandReconfigure() {
        Settings.Item item = getLastSettingsItem();
        String title = ("0000000" + item.getTitle()).substring(item.getTitle().length());
        addCommandToQueue(new BluetoothCommandRestore());
        BluetoothConnection.getDefaultInstance().commandConfigure(
                String.format(Locale.getDefault(), "%015d", App.get().getModification().getId()),
                title,
                item.getParam1(), item.getParam2(), item.getParam3(),
                getLastSettingsItemContent());
    }

    public void commandGetDevId() {
        addCommandToQueue(new BluetoothCommandGetDevId());
    }

    public void cancel() {
        if (connectionThread != null)
            connectionThread.cancel();
        defaultInstance = null;
    }

    private class ConnectionThread extends Thread {
        private InputStream mmInStream;
        private OutputStream mmOutStream;
        private BluetoothSocket mmBluetoothSocket;
        private BluetoothConnectionInterface connectionCallback;

        private Thread writeThread = null;
        private int retryCount = 5;

        public ConnectionThread(BluetoothSocket bluetoothSocket, BluetoothConnectionInterface connectionCallback) {
            this.mmBluetoothSocket = bluetoothSocket;
            this.connectionCallback = connectionCallback;
        }

        @SuppressLint("MissingPermission")
        public void run() {
            try {
                mmBluetoothSocket.connect();
                mmInStream = mmBluetoothSocket.getInputStream();
                mmOutStream = mmBluetoothSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
                connectionCallback.connectionFailed();
                cancel();
                LoggingHelper.getInstance().appendLog("error: " + "unable to connect " + e.getMessage());
                return;
            }
            byte[] buffer = new byte[1024];
            int bytes;
            new Thread() {
                public void run() {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ignored) {
                    }

                    while (getCurrStatus() == STATUS_CALIBRATING) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ignored) {
                        }
                    }
                    commandPing();
                    commandAuth();
                    commandGetDevId();
                    commandGetStatus();
                }
            }.start();

            while (!isInterrupted()) {
                try {
                    bytes = mmInStream.read(buffer);
                    String response = new String(buffer).substring(0, bytes - 1).toLowerCase().trim();
                    if (BluetoothConnection.this != getDefaultInstance()) {
                        cancel();
                        return;
                    }
                    Log.d("test", "BluetoothResponse: " + buffer.length + " bytes: " + response);
                    LoggingHelper.getInstance().appendLog("received: " + response);

                    if (writeThread != null) writeThread.interrupt();
                    //Calibrating
                    if (response.contains("cal")) {
                        commandQueue.clear();
                        Object event;

                        if (response.contains("%") && !response.contains("100%")) {
                            event = new EventBluetoothCalibratingProgress(Integer.valueOf(response.substring(response.indexOf(" ") + 1, response.indexOf("%"))));
                            currStatus = STATUS_CALIBRATING;
                        } else if (response.contains("interrupted")) {
                            event = new EventBluetoothCalibratingInterrupted();
                            currStatus = STATUS_CONNECTED;
                        } else {
                            event = new EventBluetoothCalibratingCompleted();
                            currStatus = STATUS_CONNECTED;
                        }
                        EventBus.getDefault().post(event);
                        continue;
                    }
                    //--
                    if (commandQueue.size() == 0) continue;
                    BluetoothCommand command = commandQueue.get(0);
                    //UnknownCommand
                    if (response.contains("unknown command") && retryCount > 0) {
                        retryCount--;
                        write(command);
                        continue;
                    } else {
                        retryCount = 5;
                    }
                    //Configured
                    if (command.getCode() == BluetoothCommand.COMMAND_CONFIGURE) {
                        if (response.contains("configured")) {
                            setLastMac(device.getAddress());
                            setLastName(name);
                            setLastPassword(password);
                            tunefilesLeft--;
                        }
                    }//Restore complete
                    if (command.getCode() == BluetoothCommand.COMMAND_RESTORE) {
                        if (response.contains("RESTORE COMPLETE")) {
                        }
                    }
                    //Ping
                    if (command.getCode() == BluetoothCommand.COMMAND_PING) {
                        if (!response.contains("ok")) {
                            write(command);
                            continue;
                        }
                    }
                    //Auth
                    if (command.getCode() == BluetoothCommand.COMMAND_AUTH) {
                        if (response.equals("authorized")) {
                        } else {
                            if (connectionCallback != null) {
                                connectionCallback.wrongPassword();
                                connectionCallback = null;
                            }
                            commandQueue.clear();
                            writeThread.interrupt();
                        }
                    }
                    //GetFWMode
                    if (command.getCode() == BluetoothCommand.COMMAND_GETFWMODE) {
                        deviceStatus.setFWMode(String.valueOf(response.charAt(response.indexOf("?") + 1)).toUpperCase());
                        if (deviceStatus.getFWMode().contains("A"))
                            App.get().setActivationMode(0);
                        else if (deviceStatus.getFWMode().contains("B"))
                            App.get().setActivationMode(1);
                        else
                            App.get().setActivationMode(2);
                    }
                    //GetStatus
                    if (command.getCode() == BluetoothCommand.COMMAND_GET_STATUS) {
                        if (!deviceStatus.parseStatus(response)) {
                            EventBus.getDefault().post(new EventBluetoothErrorConnectionLost());
                            commandQueue.clear();
                            cancel();
                        } else {
                            EventBus.getDefault().post(new EventBluetoothStatusReceived());
                            if (deviceStatus.getFWMode().contains("A"))
                                App.get().setActivationMode(0);
                            else if (deviceStatus.getFWMode().contains("B"))
                                App.get().setActivationMode(1);
                            else
                                App.get().setActivationMode(2);

                            if (connectionCallback != null) {
                                connectionCallback.connectionSuccess();
                                connectionCallback = null;
                            }
                        }
                    }
                    //GetDevId
                    if (command.getCode() == BluetoothCommand.COMMAND_GET_DEVICE_ID) {
                        deviceId = response.trim();
                    }

                    if (writeThread != null) writeThread.interrupt();
                    if (commandQueue.size() > 0) commandQueue.remove(0);
                    if (commandQueue.size() > 0) write(commandQueue.get(0));
                    else {
                        write(new BluetoothCommandReset());
                        currStatus = STATUS_CONNECTED;
                    }
                } catch (IOException e) {
                    cancel();
                    e.printStackTrace();
                    LoggingHelper.getInstance().appendLog("error: " + "unable to read input stream " + e.getMessage());
                    break;
                }
            }
        }

        public void write(final BluetoothCommand command) {
            currStatus = STATUS_BUSY;
            if (writeThread != null) writeThread.interrupt();
            writeThread = new Thread(new Runnable() {
                private int pingRetryCount = 5;

                @Override
                public void run() {
                    try {
                        if (mmOutStream != null) {
                            while (true) {
                                Log.d("test", "trying to send: " + command.getCommand());
                                LoggingHelper.getInstance().appendLog("sending: " + command.getCommand());
                                mmOutStream.write(command.getCommand().getBytes());
                                mmOutStream.flush();
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException ignored) {
                                    return;
                                }
                                if (commandQueue.size() > 0 && commandQueue.get(0).getCode() == BluetoothCommand.COMMAND_PING && pingRetryCount > 0) {
                                    pingRetryCount--;
                                } else {
                                    break;
                                }
                            }
                            currStatus = STATUS_ERROR;
                            commandQueue.clear();
                            cancel();
                            LoggingHelper.getInstance().appendLog("error: " + "connection timeout");
                            EventBus.getDefault().post(new EventBluetoothErrorTimeout());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        LoggingHelper.getInstance().appendLog("error: " + "unable to send command " + e.getMessage());
                        cancel();
                    }
                }
            });
            writeThread.start();
        }

        public void cancel() {
            currStatus = STATUS_DISCONNECTED;
            EventBus.getDefault().post(new EventBluetoothErrorConnectionLost());

            interrupt();
            connectionThread = null;
            defaultInstance = null;
            this.interrupt();

            if (writeThread != null) writeThread.interrupt();

            try {
                mmBluetoothSocket.close();
            } catch (Exception ignored) {
            }
            try {
                mmInStream.close();
            } catch (Exception ignored) {
            }
            try {
                mmOutStream.close();
            } catch (Exception ignored) {
            }
        }
    }
}
