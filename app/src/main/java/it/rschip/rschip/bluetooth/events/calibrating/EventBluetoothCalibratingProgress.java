package it.rschip.rschip.bluetooth.events.calibrating;

public class EventBluetoothCalibratingProgress {
    private int progress;

    public EventBluetoothCalibratingProgress(int progress) {
        this.progress = progress;
    }

    public int getProgress() {
        return progress;
    }
}
