package it.rschip.rschip.bluetooth.commands;

import android.support.annotation.NonNull;

public class BluetoothCommandAuth extends BluetoothCommand {
    private String password;

    public BluetoothCommandAuth(@NonNull String password) {
        this.password = password;
    }

    @Override
    public String getCommand() {
        return "\nAuth " + password + "\0";
    }

    @Override
    public int getCode() {
        return COMMAND_AUTH;
    }
}
