package it.rschip.rschip.bluetooth.events.errors;

public abstract class EventBluetoothError {
    public static final int ERROR_UNDEFINED = 0;
    public static final int ERROR_TIMEOUT = 1;
    public static final int ERROR_NOT_RESPONDING = 2;
    public static final int ERROR_CONNECTION_LOST = 3;

    abstract public String getDescription();

    abstract public int getCode();
}
