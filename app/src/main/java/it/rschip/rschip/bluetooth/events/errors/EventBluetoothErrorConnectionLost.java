package it.rschip.rschip.bluetooth.events.errors;

public class EventBluetoothErrorConnectionLost extends EventBluetoothError {

    @Override
    public String getDescription() {
        return "Connection lost";
    }

    @Override
    public int getCode() {
        return ERROR_CONNECTION_LOST;
    }
}