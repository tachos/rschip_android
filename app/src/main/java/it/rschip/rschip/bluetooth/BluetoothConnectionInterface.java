package it.rschip.rschip.bluetooth;

public interface BluetoothConnectionInterface {
    void connectionSuccess();

    void connectionFailed();
    void wrongPassword();
}
